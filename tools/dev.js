process.env.NODE_ENV = 'development';
require('dotenv').config();

const webpack = require('webpack');
const bs = require('browser-sync').create();
const historyApiFallback = require('connect-history-api-fallback');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const proxy = require('http-proxy-middleware');
const config = require('../config/webpack/webpack.dev.js');

const bundler = webpack(config);

bs.init({
  port: 4000,
  ui: {
    port: 4001
  },
  server: {
    baseDir: 'src',

    middleware: [
      proxy('/v1', {
        target: process.env.STAGE_URL || 'https://app.test.onederx.com',
        ws: true,
        secure: false,
        changeOrigin: true,
        logLevel: 'debug'
      }),
      historyApiFallback(),
      webpackDevMiddleware(bundler, {
        publicPath: config.output.publicPath,
        logLevel: 'error',
        stats: 'errors-only'
      }),
      webpackHotMiddleware(bundler, {
        reload: true
      })
    ]
  },
  files: [
    'src/*.html',
    'src/locales/**/*.yml'
  ]
});
