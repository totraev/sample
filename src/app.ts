import { reaction, action } from 'mobx';

import rootStore from './stores/rootStore';
import wsApi from './api/ws/api';
import appState from './ui/common/states/appState';
import Reconnection from './api/ws/reconnection';

import { L2Message, PriceLevel } from '@api/ws/types/l2updates';
import { TradeMessage, Trade } from '@api/ws/types/trades';
import { TickerMessage, TickerPayload } from '@api/ws/types/ticker';
import { BalanceMessage, Balance } from '@api/ws/types/balances';
import { PositionMessage } from '@api/ws/types/positions';
import { L3Message } from '@api/ws/types/l3Private';
import { StopOrderMessage } from '@api/ws/types/stopOrders';
import { TradePrivateMessage } from '@api/ws/types/tradesPrivate';

import { chunkThrottle, lastItemThrottle } from './utils/throttle';

reaction(() => rootStore.auth.isAuth, (isAuth) => {
  if (isAuth) {
    const { apiKey, secret } = rootStore.auth.authData;

    wsApi.auth(apiKey, secret);
  } else {
    wsApi.deauth();
    rootStore.clearPrivateStores();
    rootStore.orderBook.removeUserOrders();
  }
});

/**
 * State handlers
 */
wsApi.on('online', (): void => {
  console.log('online');
});

wsApi.on('offline', (): void => {
  console.log('offline');
});

wsApi.on('connecting', () => {
  appState.setStatus('connecting');
});

wsApi.on('connected', async () => {
  const { authData, isAuth } = rootStore.auth;

  rootStore.clear();
  await rootStore.init();

  appState.setStatus('connected');

  rootStore.currencies.symbolNames.forEach((symbol) => {
    wsApi.subscribe([
      { channel: 'l2', params: { symbol } },
      { channel: 'trades', params: { symbol } },
      { channel: 'ticker', params: { symbol } }
    ]);
  });

  if (isAuth) {
    wsApi.auth(authData.apiKey, authData.secret);
  }
});

wsApi.on('disconnected', (): void => {
  appState.setStatus('disconnected');
});

wsApi.on('auth', (): void => {
  wsApi.subscribe([
    { channel: 'balances' },
    { channel: 'positions' }
  ]);

  rootStore.currencies.symbolNames.forEach((symbol) => {
    wsApi.subscribe([
      { channel: 'l3_private', params: { symbol } },
      { channel: 'trades_private', params: { symbol } },
      { channel: 'stop_orders', params: { symbol } }
    ]);
  });
});

wsApi.on('subscribe', msg => console.log(msg));

/**
 * Data handlers
 */
const l2Throttle = chunkThrottle<PriceLevel>(action((priceLevels: PriceLevel[]) => {
  priceLevels.forEach(priceLevel => rootStore.orderBook.addPriceLvl(priceLevel));
}), 500);

wsApi.onData('l2', (msg: L2Message) => {
  if (msg.type === 'snapshot') {
    const { params: { symbol }, payload: { snapshot, updates } } = msg;

    rootStore.orderBook.addPriceLevels(symbol, snapshot.concat(updates));
    rootStore.orderBook.endLoading(symbol);
  } else {
    l2Throttle(msg.payload);
  }
});

const tradesThrottle = chunkThrottle<Trade>(action((trades: Trade[]) => {
  trades.forEach(trade => rootStore.trades.addTrade(trade));
}), 500);

wsApi.onData('trades', (msg: TradeMessage) => {
  if (msg.type === 'snapshot') {
    const { params: { symbol }, payload: trades } = msg;

    rootStore.trades.addTrades(symbol, trades);
    rootStore.trades.endLoading(symbol);
  } else {
    tradesThrottle(msg.payload);
  }
});

const tickerThrottle = lastItemThrottle<TickerPayload>(action((data: TickerPayload[]) => {
  data.forEach(payload => rootStore.currencies.onPriceStats(payload));
}), 1000);

wsApi.onData('ticker', (msg: TickerMessage) => {
  tickerThrottle(msg.payload.symbol, msg.payload);
});

const balanceThrottle = lastItemThrottle<Balance>(action((data: Balance[]) => {
  data.forEach(payload => rootStore.balances.onUpdate(payload));
}), 1000);

wsApi.onData('balances', (msg: BalanceMessage) => {
  if (msg.type === 'snapshot') {
    rootStore.balances.addBalances(msg.payload);
    rootStore.balances.endLoading();
  } else {
    balanceThrottle(msg.payload.curr, msg.payload);
  }
});

wsApi.onData('positions', (msg: PositionMessage) => {
  if (msg.type === 'snapshot') {
    rootStore.positions.setPositions(msg.payload);
    rootStore.positions.endLoading();
  } else {
    rootStore.positions.updatePosition(msg.payload);
  }
});

wsApi.onData('l3_private', (msg: L3Message) => {
  if (msg.type === 'snapshot') {
    const { snapshot, updates } = msg.payload;

    rootStore.openOrders.setSnapshot(snapshot, updates);
    rootStore.openOrders.endloading();

    const limitOrders = rootStore.openOrders.ordersArr
      .filter(order => order.type === 'limit');

    rootStore.orderBook.addUserOrders(limitOrders);
  } else {
    rootStore.openOrders.update(msg.payload);

    const order = rootStore.openOrders.ordersMap.get(msg.payload.order_id);

    if (order.type === 'limit') {
      rootStore.orderBook.updateUserOrder(order);
    }
  }
});

wsApi.onData('stop_orders', (msg: StopOrderMessage) => {
  if (msg.type === 'snapshot') {
    rootStore.stopOrders.setSnapshot(msg.payload);
    rootStore.stopOrders.endLoading();
  } else {
    rootStore.stopOrders.onUpdate(msg.payload);
  }
});

wsApi.onData('trades_private', (msg: TradePrivateMessage) => {
  if (msg.type === 'snapshot') {
    rootStore.executions.setExecutions(msg.payload);
    rootStore.executions.endLoading();
  } else {
    rootStore.executions.addExecution(msg.payload);
  }
});

export const reconnecttion = new Reconnection(wsApi);

wsApi.connect();

export default {
  rootStore,
  wsApi
};
