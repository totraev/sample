import BigNumber from 'bignumber.js';

import { nanoSecToMilliSec } from '@utils/time';

export interface TradePayload {
  taker_id: number;
  maker_id : number;
  side: 'buy' | 'sell';
  volume: string;
  price: string;
  symbol: string;
  id: number;
  event_time: number;
}

class TradeModel {
  public readonly id: number;
  public readonly symbol: string;
  public readonly volume: BigNumber;
  public readonly price: BigNumber;
  public readonly side: 'buy' | 'sell';
  public readonly takerId: number;
  public readonly makerId : number;
  public readonly timestamp: number;

  constructor(props: TradePayload) {
    this.id = props.id;
    this.symbol = props.symbol;
    this.volume = new BigNumber(props.volume);
    this.price = new BigNumber(props.price);
    this.side = props.side;
    this.takerId = props.taker_id;
    this.makerId = props.maker_id;
    this.timestamp = nanoSecToMilliSec(props.event_time);
  }
}

export default TradeModel;
