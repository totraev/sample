import { observable, computed, action } from 'mobx';
import { asyncAction } from 'mobx-utils';

import BigNumber from 'bignumber.js';
import { nanoSecToMilliSec } from '@utils/time';

import {
  L3OrderCreatedPayload,
  OrderType,
  L3Action
} from '@api/ws/types/l3Private';

import { OrdersStore } from '@stores/ordersStore';

class OrderModel {
  public readonly id: number;
  public readonly currPair: string;
  public readonly type: OrderType;
  public readonly side: 'buy' | 'sell';
  public readonly price: BigNumber = null;
  public readonly initialVolume: BigNumber;
  @observable.ref public volume: BigNumber;
  @observable public state: L3Action;
  @observable public timestamp: number;
  @observable public updated: boolean = false;

  constructor(private ordersStore: OrdersStore, payload: L3OrderCreatedPayload) {
    this.id = payload.order_id;
    this.currPair = payload.symbol;
    this.type = payload.type;
    this.side = payload.side;
    this.price = payload.type === 'limit' ? new BigNumber(payload.price) : null;
    this.initialVolume = new BigNumber(payload.initial_volume);
    this.volume = new BigNumber(payload.volume);
    this.state = payload.action;
    this.timestamp = nanoSecToMilliSec(payload.event_time);
  }

  @computed public get status(): string {
    if (this.isCanceled) {
      return this.volume.eq(this.initialVolume)
        ? 'canceled'
        : 'partial_filled';
    }

    if (this.isOpen) {
      return 'open';
    }

    if (this.isFilled) {
      return 'filled';
    }
  }

  @computed public get filledVolume(): BigNumber {
    return this.initialVolume.minus(this.volume);
  }

  @computed public get isOpen(): boolean {
    return this.state === L3Action.Created && !this.isFilled;
  }

  @computed public get isCanceled(): boolean {
    return this.state === L3Action.Canceled;
  }

  @computed public get isFilled(): boolean {
    return this.volume.isZero();
  }

  @action public updateVolume(volume: BigNumber): void {
    this.volume = volume;
  }

  @action public updateState(state: L3Action): void {
    this.state = state;
  }

  @action public updateTimestamp(value: number): void {
    this.timestamp = value;
  }

  @action public update(): void {
    this.updated = true;
  }

  @action public reset(): void {
    this.updated = false;
  }

  @asyncAction public *cancel(): any {
    yield this.ordersStore.cancelOrder({
      symbol: this.currPair,
      order_id: this.id
    });
  }
}

export default OrderModel;
