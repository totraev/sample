import { observable, computed, action } from 'mobx';
import BigNumber from 'bignumber.js';

import OrderModel from '@models/OrderModel';
import { nanoSecToMilliSec } from '@utils/time';

export interface PriceLevel {
  symbol: string;
  side: 'sell' | 'buy';
  price: string;
  volume: string;
  timestamp: number;
  seq_num?: number;
  count?: number;
}

class PriceLevelModel {
  public readonly id: string;
  public readonly side: 'buy' | 'sell';
  public readonly price: BigNumber;
  public readonly currPair: string;
  public createdFromUserOrder: boolean;
  @observable.ref public volume: BigNumber;
  @observable public timestamp: number;
  public userOrderMap = observable.map<number, OrderModel>();

  constructor(props: PriceLevel, orders: OrderModel[] = [], createdFromUserOrder: boolean = false) {
    this.id = props.price;
    this.side = props.side;
    this.price = new BigNumber(props.price);
    this.createdFromUserOrder = createdFromUserOrder;
    this.volume = new BigNumber(props.volume);
    this.timestamp = nanoSecToMilliSec(props.timestamp);

    orders.forEach(order => this.userOrderMap.set(order.id, order));
  }

  @computed public get userOrders(): OrderModel[] {
    return Array.from(this.userOrderMap.values());
  }

  @computed public get myVolume(): BigNumber {
    return this.userOrders.reduce((acc, order) => acc.plus(order.volume), new BigNumber(0));
  }

  @action public merge(priceLvl: PriceLevelModel): void {
    this.volume = priceLvl.volume;
    this.timestamp = priceLvl.timestamp;
    this.createdFromUserOrder = priceLvl.createdFromUserOrder;

    this.userOrderMap.merge(priceLvl.userOrderMap);
  }

  @action public addOrder(order: OrderModel): void {
    this.userOrderMap.set(order.id, order);
  }

  @action public removeOrder(order: OrderModel): void {
    this.userOrderMap.delete(order.id);
  }

  @action public removeOrders(): void {
    this.userOrderMap.clear();
  }
}

export default PriceLevelModel;
