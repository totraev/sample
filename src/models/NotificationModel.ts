import { NotificationStore } from '@stores/notificationStore';

export interface IProps {
  type: 'error' | 'info' | 'success';
  message: string;
  timeout?: number;
  delay?: number;
}

class NotificationModel {
  public readonly type: 'error' | 'info' | 'success';
  public readonly message: string;
  public readonly time: number = Date.now();
  private timer: NodeJS.Timer = null;

  constructor(private store: NotificationStore, props: IProps) {
    const { timeout = 4000, delay = 0, type, message } = props;

    this.type = type;
    this.message = message;

    this.timer = setTimeout(this.remove, delay + timeout);
  }

  public remove = (): void => {
    clearTimeout(this.timer);

    this.store.removeNotification(this);
  }
}

export default NotificationModel;
