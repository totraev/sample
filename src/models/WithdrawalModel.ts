import BigNumber from 'bignumber.js';

import { Withdrawal } from '@api/http/types/walletsApi';
import { nanoSecToMilliSec } from '@utils/time';

class WithdrawalModel {
  public readonly id: number;
  public readonly currency: string;
  public readonly amoung: BigNumber;
  public readonly done: boolean;
  public readonly time: number;
  public readonly address: string;
  public readonly fee: BigNumber;

  constructor(data: Withdrawal) {
    this.id = data.trans_id;
    this.currency = data.curr;
    this.amoung = new BigNumber(data.amount);
    this.done = data.done;
    this.time = nanoSecToMilliSec(data.time);
    this.fee = new BigNumber(data.fee);
    this.address = data.dest_address;
  }
}

export default WithdrawalModel;
