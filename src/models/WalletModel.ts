import { observable, action } from 'mobx';

import { Wallet } from '@api/http/types/walletsApi';

class WalletModel {
  public readonly currency: string;
  @observable public address: string;

  constructor(props: Wallet) {
    this.currency = props.curr;
    this.address = props.address;
  }

  @action public updateAddress(value: string): void {
    this.address = value;
  }
}

export default WalletModel;
