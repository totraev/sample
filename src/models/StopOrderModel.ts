import { observable, action } from 'mobx';
import { asyncAction } from 'mobx-utils';
import BigNumber from 'bignumber.js';

import { StopOrderAccepted } from '@api/ws/types/stopOrders';
import { StopOrdersStore } from '@stores/stopOrdersStore';

export type StopState = 'accepted' | 'refused' | 'deleted' | 'triggered';

class StopOrderModel {
  public readonly id: number;
  public readonly currPair: string;
  public readonly price: BigNumber;
  public readonly volume: BigNumber;
  public readonly side: 'sell' | 'buy';
  public readonly timestamp: number;
  @observable public state: StopState;
  @observable public updated: boolean = false;

  constructor(private stopOrdersStore: StopOrdersStore, data: StopOrderAccepted) {
    this.id = data.order_id;
    this.currPair = data.symbol;
    this.price = new BigNumber(data.price);
    this.volume = new BigNumber(data.volume);
    this.state = data.action;
    this.side = data.side;
    this.timestamp = data.event_time / (1000 * 1000);
  }

  @action public updateState(state: StopState): void {
    this.state = state;
  }

  @action public update(): void {
    this.updated = true;
  }

  @action public reset(): void {
    this.updated = false;
  }

  public remove = (): void => {
    this.stopOrdersStore.remove(this.id);
  }

  @asyncAction public *cancel(): IterableIterator<Promise<void>> {
    yield* this.stopOrdersStore.cancelOrder({
      symbol: this.currPair,
      order_id: this.id
    });
  }
}

export default StopOrderModel;
