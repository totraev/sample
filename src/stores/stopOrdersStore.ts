import { observable, computed, action } from 'mobx';
import { asyncAction } from 'mobx-utils';

import { StopOrderAccepted, StopOrderPayload } from '@api/ws/types/stopOrders';
import { cancelOrder, cancelAllStopOrders, CancelOrderPayload } from '@api/http/ordersApi';

import StopOrderModel from '@models/StopOrderModel';

import { RootStore } from '@stores/rootStore';

export class StopOrdersStore {
  @observable public loading: boolean = true;
  public stopOrders = observable.map<number, StopOrderModel>({});

  constructor(private rootStore: RootStore) {}

  @computed public get stopOrdersArr(): StopOrderModel[] {
    return Array.from(this.stopOrders.values());
  }

  @action public startLoading(): void {
    this.loading = true;
  }

  @action public endLoading(): void {
    this.loading = false;
  }

  @action public setSnapshot(stopOrders: StopOrderAccepted[]): void {
    stopOrders.forEach(stopOrder =>
      this.stopOrders.set(stopOrder.order_id, new StopOrderModel(this, stopOrder))
    );
  }

  @action public onUpdate(data: StopOrderPayload): void {
    if (data.action === 'accepted') {
      const stopOrder = new StopOrderModel(this, data);

      stopOrder.update();
      this.stopOrders.set(data.order_id, stopOrder);
    } else {
      const stopOrder = this.stopOrders.get(data.order_id);

      stopOrder.updateState(data.action);
      stopOrder.update();
    }
  }

  @action public remove(orderId: number): void {
    this.stopOrders.delete(orderId);
  }

  @action public clear(): void {
    this.stopOrders.clear();
    this.loading = true;
  }

  @asyncAction public *cancelOrder(data: CancelOrderPayload): IterableIterator<Promise<void>> {
    try {
      yield cancelOrder(data);
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }

  @asyncAction public *cancelAllOrders(): IterableIterator<Promise<void[]>> {
    const { symbolNames } = this.rootStore.currencies;

    try {
      yield Promise.all(symbolNames.map(currPair => cancelAllStopOrders(currPair)));
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }
}

export default StopOrdersStore;
