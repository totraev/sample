import { observable, action } from 'mobx';

import { PriceLevel } from '@api/ws/types/l2updates';
import PriceLevelModel from '@models/PriceLevelModel';
import OrderBookService from '@services/OrderBookService';
import OrderModel from '@models/OrderModel';

export class OrderBookStore {
  public orderBookMap = observable.map<string, OrderBookService>({}, { deep: false });

  @action public init(symbols: string[]): void {
    symbols.forEach(symbol => this.orderBookMap.set(symbol, new OrderBookService(symbol)));
  }

  public startLoading(symbol: string): void {
    if (this.orderBookMap.has(symbol)) {
      this.orderBookMap.get(symbol).startLoading();
    }
  }

  public endLoading(symbol: string): void {
    if (this.orderBookMap.has(symbol)) {
      this.orderBookMap.get(symbol).endLoading();
    }
  }

  @action public addPriceLevels(symbol: string, priceLvls: PriceLevel[]): void {
    const orderBook = this.orderBookMap.get(symbol);

    priceLvls.forEach(priceLvl => orderBook.addPriceLevel(new PriceLevelModel(priceLvl)));
  }

  @action public addPriceLvl(priceLvl: PriceLevel): void {
    this.orderBookMap.get(priceLvl.symbol).addPriceLevel(new PriceLevelModel(priceLvl));
  }

  @action public addUserOrders(orders: OrderModel[]): void {
    orders.forEach((order) => {
      if (order.type === 'limit' && order.isOpen) {
        this.orderBookMap.get(order.currPair).addUserOrder(order);
      }
    });
  }

  @action public updateUserOrder(order: OrderModel): void {
    if (order.type !== 'limit') return;

    if (order.isOpen) {
      return this.orderBookMap.get(order.currPair).addUserOrder(order);
    }

    if (order.isCanceled || order.isFilled) {
      return this.orderBookMap.get(order.currPair).removeUserOrder(order);
    }
  }

  public clear(): void {
    this.orderBookMap.forEach((orderBook) => {
      orderBook.clear();
      orderBook.startLoading();
    });
  }

  public removeUserOrders(): void {
    this.orderBookMap.forEach(orderBook => orderBook.removeUserOrders());
  }
}

export default OrderBookStore;
