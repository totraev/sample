import { observable, computed, action } from 'mobx';
import { asyncAction } from 'mobx-utils';
import BigNumber from 'bignumber.js';

import {
  L3OrderCreatedPayload,
  L3OrderExecutedPayload,
  L3OrderCanceledPayload,
  L3UpdatePayload,
  L3Action
} from '@api/ws/types/l3Private';

import {
  cancelOrder,
  cancelAllOrders,
  CancelOrderPayload
} from '@api/http/ordersApi';

import OrderModel from '@models/OrderModel';
import { RootStore } from '@stores/rootStore';

export class OrdersStore {
  @observable public loading: boolean = true;
  public ordersMap = observable.map<number, OrderModel>({});

  constructor(private rootStore: RootStore) {}

  @computed public get ordersArr(): OrderModel[] {
    return Array.from(this.ordersMap.values());
  }

  @action public startLoading(): void {
    this.loading = true;
  }

  @action public endloading(): void {
    this.loading = false;
  }

  @action public setSnapshot(snapshot: L3OrderCreatedPayload[], updates: L3UpdatePayload[]): void {
    snapshot.forEach(data => this.create(data, false));
    updates.forEach(update => this.update(update, false));
  }

  public update(payload: L3UpdatePayload, update: boolean = true): void {
    switch (payload.action) {
      case L3Action.Created:
        return this.create(payload, update);
      case L3Action.Executed:
        return this.execute(payload, update);
      case L3Action.Canceled:
        return this.cancel(payload, update);
    }
  }

  @action private create(payload: L3OrderCreatedPayload, update: boolean = true): void {
    const order = new OrderModel(this, payload);
    this.ordersMap.set(payload.order_id, order);

    if (update) {
      order.update();
    }
  }

  @action private execute(payload: L3OrderExecutedPayload, update: boolean = true): void {
    const order = this.ordersMap.get(payload.order_id);

    order.updateVolume(new BigNumber(payload.volume));

    if (update) {
      order.update();
    }
  }

  @action private cancel(payload: L3OrderCanceledPayload, update: boolean = true): void {
    const order = this.ordersMap.get(payload.order_id);
    order.updateState(payload.action);

    if (update) {
      order.update();
    }
  }

  @action public clear(): void {
    this.ordersMap.clear();
    this.loading = true;
  }

  @asyncAction public *cancelOrder(data: CancelOrderPayload): any {
    try {
      yield cancelOrder(data);
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }

  @asyncAction public *cancelAllOrders(): any {
    const { symbolNames } = this.rootStore.currencies;

    try {
      yield Promise.all(symbolNames.map(currPair => cancelAllOrders(currPair)));
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }
}

export default OrdersStore;
