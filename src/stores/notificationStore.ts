import { observable, action } from 'mobx';

import NotificationModel from '@models/NotificationModel';

export class NotificationStore {
  public notifications = observable.array<NotificationModel>([], { deep: false });
  private readonly minDelay = 300;
  private queueCount = 0;

  public addErrorNotification(message: string): void {
    this.addNotificationToQueue(message, 'error');
  }

  public addInfoNotification(message: string): void {
    this.addNotificationToQueue(message, 'info');
  }

  public addSuccessNotification(message: string): void {
    this.addNotificationToQueue(message, 'success');
  }

  @action public removeNotification(notification: NotificationModel): void {
    this.notifications.remove(notification);
    this.queueCount -= 1;
  }

  @action public addNotification(notification: NotificationModel): void {
    this.notifications.push(notification);
  }

  private addNotificationToQueue(message: string, type: 'error' | 'info' | 'success'): void {
    const delay = this.queueCount * this.minDelay;
    const notificationProps = { message, type, delay };

    setTimeout(() => this.addNotification(new NotificationModel(this, notificationProps)), delay);
    this.queueCount += 1;
  }
}

export default NotificationStore;
