import { observable, action } from 'mobx';

import { Trade } from '@api/ws/types/trades';
import TradeModel from '@models/TradeModel';
import TradeDataService from '@services/TradeDataService';

export class TradesStore {
  public tradeMap = observable.map<string, TradeDataService>();

  @action public init(symbols: string[]): void {
    symbols.forEach(symbol => this.tradeMap.set(symbol, new TradeDataService(symbol)));
  }

  public startLoading(symbol: string): void {
    if (this.tradeMap.has(symbol)) {
      this.tradeMap.get(symbol).startLoading();
    }
  }

  public endLoading(symbol: string): void {
    if (this.tradeMap.has(symbol)) {
      this.tradeMap.get(symbol).endLoading();
    }
  }

  public addTrades(symbol: string, trades: Trade[]): void {
    const tradeModels = trades.map(trade => new TradeModel(trade));

    this.tradeMap.get(symbol).setTrades(tradeModels);
  }

  public addTrade(trade: Trade): void {
    this.tradeMap.get(trade.symbol).addTrade(new TradeModel(trade));
  }

  public clear(): void {
    this.tradeMap.forEach((tradeData) => {
      tradeData.clear();
      tradeData.startLoading();
    });
  }
}

export default TradesStore;
