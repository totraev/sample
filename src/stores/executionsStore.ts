import { observable, action } from 'mobx';

import { Trade } from '@api/ws/types/tradesPrivate';
import ExecutionModel from '@models/ExecutuinModel';

export class ExecutionsStore {
  @observable public loading: boolean = true;
  public executions = observable.array<ExecutionModel>([], { deep: false });

  @action public startLoading(): void {
    this.loading = true;
  }

  @action public endLoading(): void {
    this.loading = false;
  }

  @action public setExecutions(executions: Trade[]): void {
    executions.forEach((execution) => {
      this.executions.push(new ExecutionModel(execution));
    });
  }

  @action public addExecution(execution: Trade): void {
    const trade = new ExecutionModel(execution);

    this.executions.push(trade);
    trade.update();
  }

  @action public clear(): void {
    this.executions.clear();
    this.loading = true;
  }
}

export default ExecutionsStore;
