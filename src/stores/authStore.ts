import { observable, action, computed } from 'mobx';
import { asyncAction } from 'mobx-utils';

import * as authApi from '@api/http/authApi';
import { TwoFaEnabledResponse, TwoFaEnableResponse, TokenResponse } from '@api/http/types/authApi';

import analyticService from '@services/AnalyticService';

export interface AuthData {
  email: string;
  apiKey: string;
  secret: string;
  userId: string;
}

export class AuthStore {
  @observable public twoFaEnabled: boolean = false;
  @observable public authData: AuthData = {
    email: localStorage.getItem('email'),
    apiKey: localStorage.getItem('apiKey'),
    secret: localStorage.getItem('secret'),
    userId: localStorage.getItem('userId')
  };

  constructor() {
    window.addEventListener(
      'storage',
      action<StorageEvent, void, (arg: StorageEvent) => void>((e) => {
        switch (e.key) {
          case 'email':
            return this.authData.email = e.newValue;
          case 'apiKey':
            return this.authData.apiKey = e.newValue;
          case 'secret':
            return this.authData.secret = e.newValue;
          case 'userId':
            return this.authData.userId = e.newValue;
        }
      })
    );
  }

  @computed public get isAuth(): boolean {
    return Boolean(this.authData.apiKey) && Boolean(this.authData.secret);
  }

  @action public setAuthData(data: AuthData): void {
    this.authData.email = data.email;
    this.authData.apiKey = data.apiKey;
    this.authData.secret = data.secret;
    this.authData.userId = data.userId;

    localStorage.setItem('email', this.authData.email);
    localStorage.setItem('apiKey', this.authData.apiKey);
    localStorage.setItem('secret', this.authData.secret);
    localStorage.setItem('userId', this.authData.userId);

    analyticService.setUser(this.authData.userId);
  }

  @action public reset(): void {
    this.authData.email = null;
    this.authData.apiKey = null;
    this.authData.secret = null;
    this.authData.userId = null;

    localStorage.removeItem('email');
    localStorage.removeItem('apiKey');
    localStorage.removeItem('secret');
    localStorage.removeItem('userId');
  }

  public async register(
    email: string,
    password: string,
    repeatPassword: string,
    recaptcha: string
  ): Promise<void> {
    const { api_key : loginResponse } = await authApi.register({
      email,
      password,
      password_repeat: repeatPassword,
      g_recaptcha_response: recaptcha
    });

    this.setAuthData({
      email,
      apiKey: loginResponse.id,
      secret: loginResponse.secret,
      userId: loginResponse.opaque_user_id
    });
  }

  public async login(
    email: string,
    password: string,
    recaptcha: string,
    code?: string
  ): Promise<void> {
    const loginResponse = await authApi.login({
      email,
      password,
      g_recaptcha_response: recaptcha,
      pass_code: code
    });

    this.setAuthData({
      email,
      apiKey: loginResponse.id,
      secret: loginResponse.secret,
      userId: loginResponse.opaque_user_id
    });
  }

  @asyncAction public *logout() {
    yield authApi.logout();

    this.reset();
  }

  public newPassword(email: string, recaptcha: string, code?: string): Promise<void> {
    return authApi.newPassword({
      email,
      g_recaptcha_response: recaptcha,
      pass_code: code
    });
  }

  public checkToken(token: string): Promise<TokenResponse> {
    return authApi.checkToken({ token });
  }

  public setPassword(
    token: string,
    password: string,
    repeatPassword: string,
    code?: string
  ): Promise<void> {
    return authApi.setPassword({
      token,
      password,
      password_repeat: repeatPassword,
      pass_code: code
    });
  }

  public changePassword(
    oldPass: string,
    newPass: string,
    repeatPass: string,
    code?: string
  ): Promise<void> {
    return authApi.changePassword({
      old_password: oldPass,
      new_password: newPass,
      new_password_repeat: repeatPass,
      pass_code: code
    });
  }

  public twoFaGenerate(): Promise<TwoFaEnableResponse> {
    return authApi.twoFaGenerate();
  }

  @asyncAction public *twoFaStatus() {
    const res: TwoFaEnabledResponse = yield authApi.twoFaStatus();
    this.twoFaEnabled = res['2fa_enabled'];
  }

  @asyncAction public *twoFaEnable(code: string) {
    yield authApi.twoFaEnable(code);
    this.twoFaEnabled = true;
  }

  @asyncAction public *twoFaDisable(code: string) {
    yield authApi.twoFaDisable(code);
    this.twoFaEnabled = false;
  }
}

export default AuthStore;
