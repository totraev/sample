import NotificationStore from './notificationStore';
import ApiKeysStore from './apiKeysStore';
import AuthStore from './authStore';
import BalanceStore from './balanceStore';
import CurrencyStore from './currencyStore';
import ExecutionsStore from './executionsStore';
import OrderBookStore from './orderBookStore';
import OrderFormStore from './orderFormStore';
import OrdersStore from './ordersStore';
import PositionsStore from './positionsStore';
import StopOrdersStore from './stopOrdersStore';
import TradesStore from './tradesStore';
import UserStore from './userStore';
import WalletStore from './walletStore';

export class RootStore {
  public notifications = new NotificationStore();
  public orderForm = new OrderFormStore();

  public orderBook = new OrderBookStore();
  public trades = new TradesStore();
  public currencies = new CurrencyStore(this);

  public executions = new ExecutionsStore();
  public positions = new PositionsStore(this);
  public openOrders = new OrdersStore(this);
  public stopOrders = new StopOrdersStore(this);

  public auth = new AuthStore();
  public user = new UserStore(this);
  public wallets = new WalletStore(this);
  public balances = new BalanceStore();
  public apiKeys = new ApiKeysStore(this);

  public async init(): Promise<void> {
    await this.currencies.fetchSymbols();

    this.orderBook.init(this.currencies.symbolNames);
    this.trades.init(this.currencies.symbolNames);
  }

  public clear(): void {
    this.orderBook.clear();
    this.trades.clear();

    this.clearPrivateStores();
  }

  public clearPrivateStores(): void {
    this.executions.clear();
    this.positions.clear();
    this.openOrders.clear();
    this.stopOrders.clear();
    this.balances.clear();

    // this.user.clear();
    // this.wallets.clear();
    // this.apiKeys.clear();
  }
}

export default new RootStore();
