import { observable, computed, action } from 'mobx';
import { asyncAction } from 'mobx-utils';

import { Position } from '@api/ws/types/positions';
import { indexTerminate, createOrder, CreateMarketOrder } from '@api/http/ordersApi';

import { RootStore } from '@stores/rootStore';

import PositionModel from '@models/PositionModel';

export class PositionsStore {
  @observable public loading: boolean = true;
  public positionsMap = observable.map<string, PositionModel>({});

  constructor(private rootStore: RootStore) {}

  @action public startLoading(): void {
    this.loading = true;
  }

  @action public endLoading(): void {
    this.loading = false;
  }

  @computed public get positions(): PositionModel[] {
    return Array.from(this.positionsMap.values());
  }

  @action public setPositions(positions: Position[]): void {
    positions.forEach((position) => {
      this.positionsMap.set(position.symbol, new PositionModel(position));
    });
  }

  @action public updatePosition(position: Position): void {
    if (this.positionsMap.has(position.symbol)) {
      this.positionsMap.get(position.symbol).update(position);
    } else {
      this.positionsMap.set(position.symbol, new PositionModel(position));
    }
  }

  @action public clear(): void {
    this.positionsMap.clear();
    this.loading = true;
  }

  @asyncAction public *terminateByIndex(symbol: string) {
    try {
      yield indexTerminate(symbol);
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }

  @asyncAction public *terminateByMarket(symbol: string) {
    const { volume } = this.positionsMap.get(symbol);
    const marketOrder: CreateMarketOrder = {
      symbol,
      volume: volume.abs().toString(),
      side: volume.isNegative() ? 'buy' : 'sell',
      type: 'market',
      stop: false
    };

    try {
      yield createOrder(marketOrder);
    } catch (e) {
      this.rootStore.notifications.addErrorNotification(e.message);
    }
  }
}

export default PositionsStore;
