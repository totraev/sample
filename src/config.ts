export default {
  ssl: Boolean(process.env.SSL),
  apiHost: process.env.API_HOST,
  apiVersion: process.env.API_VERSION,
  defaultSymbol: process.env.DEFAULT_SYMBOL,
  recaptchaSiteKey: process.env.RECAPTCHA_SITE_KEY,
  docsUrl: process.env.DOCS_URL,
  specUrl: process.env.SPEC_URL,
  ymID: process.env.YM_ID
};
