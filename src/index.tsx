import React from 'react';
import ReactDOM from 'react-dom';
import { configure } from 'mobx';
import { Provider } from 'mobx-react';
import { BrowserRouter } from 'react-router-dom';

import './assets/fonts/stylesheet.css';
import './assets/css/main.sss';

import './i18n';
import './utils/customValidators';

import App from './ui/App';
import app from './app';

import appState from './ui/common/states/appState';
import headerState from './ui/common/states/headerState';
import notificationsState from './ui/common/states/notificationsState';

import analyticService from './services/AnalyticService';

configure({ enforceActions: 'observed' });

ReactDOM.render(
  <Provider
    rootStore={app.rootStore}
    headerState={headerState}
    notificationsState={notificationsState}
    appState={appState}
    analyticService={analyticService}
  >
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>,
  document.getElementById('app')
);
