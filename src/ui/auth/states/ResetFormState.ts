import { observable, action, autorun } from 'mobx';
import { asyncAction } from 'mobx-utils';
import Validator from 'validatorjs';

import rootStore, { RootStore } from '@stores/rootStore';
import { CodeValue } from '@ui/common/components/TwoFactorCode/components/CodeInput/types';

import { INVALID_PASS_CODE, INVALID_RECAPTCHA } from '@api/http/core/errorCodes';

export interface ResetFields {
  email: string;
  recaptcha: string;
  code: CodeValue;
}

export interface ResetErrors {
  email: string;
  code: string;
}

export class ResetFormState {
  @observable public twoFaEnabled: boolean = false;
  @observable public loading: boolean = false;
  @observable public fields: ResetFields = {
    email: '',
    recaptcha: '',
    code: ['', '', '', '', '', '']
  };
  @observable public errors: ResetErrors = {
    email: '',
    code: ''
  };
  @observable public serverError: string = '';
  @observable public success: boolean = false;

  private validator: Validator.Validator<ResetErrors> = null;

  constructor(private rootStore: RootStore) {
    autorun(() => {
      this.validator = new Validator<ResetErrors>({
        email: this.fields.email,
        code: this.fields.code.join('')
      }, {
        email: 'required|email',
        code: this.twoFaEnabled ? 'required|digits:6' : ''
      });
    });
  }

  @action public updateField(name: 'email', value: string): void {
    this.fields[name] = value;

    if (Boolean(this.errors[name])) {
      this.errors[name] = '';
      this.serverError = '';
    }
  }

  @action public updateRecaptcha(value: string): void {
    this.fields.recaptcha = value;

    if (Boolean(this.serverError)) {
      this.serverError = '';
    }
  }

  @action public updateCode(value: CodeValue): void {
    this.fields.code = value;

    if (Boolean(this.errors.code) || Boolean(this.serverError)) {
      this.errors.code = '';
      this.serverError = '';
    }
  }

  @action public resetState(): void {
    this.twoFaEnabled = false;
    this.fields = {
      email: '',
      code: ['', '', '', '', '', ''],
      recaptcha: ''
    };
    this.errors = {
      email: '',
      code: ''
    };
    this.serverError = '';
    this.loading = false;
    this.success = false;
  }

  @asyncAction public *submitForm(): IterableIterator<any> {
    const { email, code, recaptcha } = this.fields;

    if (!this.validator.check()) {
      this.errors = {
        email: this.validator.errors.first('email') as string,
        code: this.validator.errors.first('code') as string
      };

      return;
    }

    this.loading = true;

    try {
      yield this.rootStore.auth.newPassword(email, recaptcha, code.join(''));

      this.success = true;
    } catch (e) {
      if (e.code === INVALID_PASS_CODE && !this.twoFaEnabled) {
        return this.twoFaEnabled = true;
      }

      if (e.code === INVALID_RECAPTCHA) {
        this.fields.recaptcha = '';
      }

      this.serverError = e.message;
    } finally {
      this.loading = false;
    }
  }
}

export default new ResetFormState(rootStore);
