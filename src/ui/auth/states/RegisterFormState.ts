import { observable, action, autorun } from 'mobx';
import { asyncAction } from 'mobx-utils';
import Validator from 'validatorjs';

import rootStore, { RootStore } from '@stores/rootStore';

import { INVALID_RECAPTCHA } from '@api/http/core/errorCodes';

export interface RegisterFields {
  email: string;
  password: string;
  repeatPassword: string;
  recaptcha: string;
}

export interface RegisterErrors {
  email: string;
  password: string;
  repeatPassword: string;
}

export class RegisterFormState {
  @observable public loading: boolean = false;
  @observable public fields: RegisterFields = {
    email: '',
    password: '',
    repeatPassword: '',
    recaptcha: ''
  };
  @observable public errors: RegisterErrors = {
    email: '',
    password: '',
    repeatPassword: ''
  };
  @observable public serverError: string = '';
  @observable public redirect: boolean = false;

  private readonly rules: RegisterErrors = {
    email: 'required|email',
    password: 'required|min:6|same:repeatPassword',
    repeatPassword: 'required|same:password'
  };
  private validator = new Validator<RegisterErrors>(this.fields, this.rules);

  constructor(private rootStore: RootStore) {
    autorun(() => {
      this.validator = new Validator<RegisterErrors>({
        email: this.fields.email,
        password: this.fields.password,
        repeatPassword: this.fields.repeatPassword
      }, this.rules);
    });
  }

  @action public updateField(name: keyof RegisterFields, value: string): void {
    this.fields[name] = value;

    if (Boolean(this.errors[name])) {
      this.errors[name] = '';
      this.serverError = '';
    }
  }

  @action public updateRecaptcha(value: string): void {
    this.fields.recaptcha = value;

    if (Boolean(this.serverError)) {
      this.serverError = '';
    }
  }

  @action public resetState(): void {
    this.fields = {
      email: '',
      password: '',
      repeatPassword: '',
      recaptcha: ''
    };
    this.errors = {
      email: '',
      password: '',
      repeatPassword: ''
    };
    this.redirect = false;
    this.serverError = '';
    this.loading = false;
  }

  @asyncAction public *submitForm(): IterableIterator<any> {
    const { email, password, repeatPassword, recaptcha } = this.fields;

    if (!this.validator.check()) {
      this.errors = {
        email: this.validator.errors.first('email') as string,
        password: this.validator.errors.first('password') as string,
        repeatPassword: this.validator.errors.first('repeatPassword') as string,
      };

      return;
    }

    this.loading = true;

    try {
      yield this.rootStore.auth.register(email, password, repeatPassword, recaptcha);

      this.redirect = true;
    } catch (e) {
      if (e.code === INVALID_RECAPTCHA) {
        this.fields.recaptcha = '';
      }

      this.serverError = e.message;
    } finally {
      this.loading = false;
    }
  }
}

export default new RegisterFormState(rootStore);
