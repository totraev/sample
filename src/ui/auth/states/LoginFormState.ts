import { observable, action, autorun } from 'mobx';
import { asyncAction } from 'mobx-utils';
import Validator from 'validatorjs';

import rootStore, { RootStore } from '@stores/rootStore';
import { CodeValue } from '@ui/common/components/TwoFactorCode/types';

import { INVALID_PASS_CODE, INVALID_RECAPTCHA } from '@api/http/core/errorCodes';

export interface LoginFields {
  email: string;
  password: string;
  code: CodeValue;
  recaptcha: string;
}

export interface LoginErrors {
  email: string;
  password: string;
  code: string;
}

export class LoginFormState {
  @observable public twoFaEnabled: boolean = false;
  @observable public loading: boolean = false;
  @observable public fields: LoginFields = {
    email: '',
    password: '',
    code: ['', '', '', '', '', ''],
    recaptcha: ''
  };
  @observable public errors: LoginErrors = {
    email: '',
    password: '',
    code: ''
  };
  @observable public serverError: string = '';
  @observable public redirect: boolean = false;

  private validator: Validator.Validator<LoginErrors> = null;

  constructor(private rootStore: RootStore) {
    autorun(() => {
      this.validator = new Validator<LoginErrors>({
        email: this.fields.email,
        password: this.fields.password,
        code: this.fields.code.join('')
      }, {
        email: 'required|email',
        password: 'required|min:6',
        code: this.twoFaEnabled ? 'required|digits:6' : ''
      });
    });
  }

  @action public updateField(name: 'email' | 'password', value: string): void {
    this.fields[name] = value;

    if (Boolean(this.errors[name]) || Boolean(this.serverError)) {
      this.errors[name] = '';
      this.serverError = '';
    }
  }

  @action public updateCode(value: CodeValue): void {
    this.fields.code = value;

    if (Boolean(this.errors.code) || Boolean(this.serverError)) {
      this.errors.code = '';
      this.serverError = '';
    }
  }

  @action public updateRecaptcha(value: string): void {
    this.fields.recaptcha = value;

    if (Boolean(this.serverError)) {
      this.serverError = '';
    }
  }

  @action public resetState(): void {
    this.twoFaEnabled = false;
    this.fields = {
      email: '',
      password: '',
      code: ['', '', '', '', '', ''],
      recaptcha: ''
    };
    this.errors = {
      email: '',
      password: '',
      code: ''
    };
    this.redirect = false;
    this.serverError = '';
    this.loading = false;
  }

  @asyncAction public *submitForm(): IterableIterator<any> {
    const { email, password, code, recaptcha } = this.fields;

    if (!this.validator.check()) {
      this.errors = {
        email: this.validator.errors.first('email') as string,
        password: this.validator.errors.first('password') as string,
        code: this.validator.errors.first('code') as string
      };

      return;
    }

    this.loading = true;

    try {
      yield this.rootStore.auth.login(email, password, recaptcha, code.join(''));

      this.redirect = true;
    } catch (e) {
      if (e.code === INVALID_PASS_CODE && !this.twoFaEnabled) {
        return this.twoFaEnabled = true;
      }

      if (e.code === INVALID_RECAPTCHA) {
        this.fields.recaptcha = '';
      }

      this.serverError = e.message;
    } finally {
      this.loading = false;
    }
  }
}

export default new LoginFormState(rootStore);
