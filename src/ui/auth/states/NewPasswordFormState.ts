import { observable, action, autorun } from 'mobx';

import { asyncAction } from 'mobx-utils';
import Validator from 'validatorjs';
import jwtDecode from 'jwt-decode';

import rootStore, { RootStore } from '@stores/rootStore';
import { CodeValue } from '@ui/common/components/TwoFactorCode/components/CodeInput/types';

export interface NewPasswordFields {
  email?: string;
  token?: string;
  password: string;
  repeatPassword: string;
  code: CodeValue;
}

export interface NewPasswordErrors {
  password: string;
  repeatPassword: string;
  code: string;
}

export interface IJWTData {
  email: string;
  token: string;
}

export class NewPasswordFormState {
  @observable public twoFaEnabled: boolean = false;
  @observable public loading: boolean = false;
  @observable public fields: NewPasswordFields = {
    email: '',
    password: '',
    repeatPassword: '',
    code: ['', '', '', '', '', '']
  };
  @observable public errors: NewPasswordErrors = {
    password: '',
    repeatPassword: '',
    code: ''
  };
  @observable public serverError: string = '';
  @observable public success: boolean = false;

  private validator: Validator.Validator<NewPasswordErrors> = null;

  constructor(public rootStore: RootStore) {
    autorun(() => {
      this.validator = new Validator<NewPasswordErrors>({
        password: this.fields.password,
        repeatPassword: this.fields.repeatPassword,
        code: this.fields.code.join('')
      }, {
        password: 'required|min:6|same:repeatPassword',
        repeatPassword: 'required|same:password',
        code: this.twoFaEnabled ? 'required|digits:6' : ''
      });
    });
  }

  @action public updateField(name: keyof NewPasswordFields, value: string): void {
    this.fields[name] = value;

    if (Boolean(this.errors[name])) {
      this.errors[name] = '';
      this.serverError = '';
    }
  }

  @action public updateCode(value: CodeValue): void {
    this.fields.code = value;

    if (Boolean(this.errors.code)) {
      this.errors.code = '';
      this.serverError = '';
    }
  }

  @asyncAction public *setEmailFromToken(jwt: string) {
    try {
      const { email, token } = jwtDecode<IJWTData>(jwt);

      this.updateField('email', email);
      this.updateField('token', token);

      yield this.checkToken(token);
    } catch (e) {
      this.serverError = e.message;
    }
  }

  @action public resetState(): void {
    this.twoFaEnabled = false;
    this.fields = {
      email: '',
      password: '',
      repeatPassword: '',
      code: ['', '', '', '', '', '']
    };
    this.errors = {
      password: '',
      repeatPassword: '',
      code: ''
    };
    this.serverError = '';
    this.loading = false;
    this.success = false;
  }

  @asyncAction public *submitForm(): IterableIterator<any> {
    const { token, password, repeatPassword, code } = this.fields;

    if (!this.validator.check()) {
      this.errors = {
        password: this.validator.errors.first('password') as string,
        repeatPassword: this.validator.errors.first('repeatPassword') as string,
        code: this.validator.errors.first('code') as string
      };

      return;
    }

    this.loading = true;

    try {
      yield this.rootStore.auth.setPassword(
        token,
        password,
        repeatPassword,
        code.join('')
      );

      this.success = true;
    } catch (e) {
      if (e.code === 19 && !this.twoFaEnabled) {
        this.twoFaEnabled = true;
      } else {
        this.serverError = e.message;
      }
    } finally {
      this.loading = false;
    }
  }

  @asyncAction public *checkToken(token: string) {
    try {
      yield this.rootStore.auth.checkToken(token);
    } catch (e) {
      this.serverError = e.message;
    }
  }
}

export default new NewPasswordFormState(rootStore);
