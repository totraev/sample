import React, { SFC } from 'react';
import { Provider } from 'mobx-react';
import { Switch, Redirect } from 'react-router-dom';

import AuthRoute from '@ui/auth/components/AuthRoute';

import LoginPage from '../LoginPage';
import RegisterPage from '../RegisterPage';
import ResetPasswordPage from '../ResetPasswordPage';
import NewPasswordPage from '../NewPasswordPage';

import loginFormState from '../../states/LoginFormState';
import registerFormState from '../../states/RegisterFormState';
import resetFormState from '../../states/ResetFormState';
import newPasswordFormState from '../../states/NewPasswordFormState';

import { IProps } from './types';

const AuthPage: SFC<IProps> = ({ match }) => (
  <Provider
    loginFormState={loginFormState}
    registerFormState={registerFormState}
    resetFormState={resetFormState}
    newPasswordFormState={newPasswordFormState}
  >
    <Switch>
      <Redirect exact from={match.path} to={`${match.path}/login`}/>
      <AuthRoute path={`${match.path}/login`} component={LoginPage}/>
      <AuthRoute path={`${match.path}/register`} component={RegisterPage}/>
      <AuthRoute path={`${match.path}/reset-password`} component={ResetPasswordPage}/>
      <AuthRoute path={`${match.path}/new-password`} component={NewPasswordPage}/>
    </Switch>
  </Provider>
);

export default AuthPage;
