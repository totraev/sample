import React, { SFC } from 'react';

import CenteredPageLayout from '@ui/common/components/CenteredPageLayout';
import LoginForm from '@ui/auth/components/LoginForm';

const LoginPage: SFC = () => (
  <CenteredPageLayout>
    <LoginForm/>
  </CenteredPageLayout>
);

export default LoginPage;
