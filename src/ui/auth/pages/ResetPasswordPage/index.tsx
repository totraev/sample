import React, { SFC } from 'react';

import CenteredPageLayout from '@ui/common/components/CenteredPageLayout';
import ResetPasswordForm from '@ui/auth/components/ResetPasswordForm';

const ResetPasswordPage: SFC = () => (
  <CenteredPageLayout>
    <ResetPasswordForm/>
  </CenteredPageLayout>
);

export default ResetPasswordPage;
