import { RouteComponentProps } from 'react-router-dom';
import { NewPasswordFormState } from '@ui/auth/states/NewPasswordFormState';

export interface IProps extends RouteComponentProps<void> {
  newPasswordFormState?: NewPasswordFormState;
}
