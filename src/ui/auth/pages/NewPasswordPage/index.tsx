import React, { PureComponent } from 'react';
import { inject } from 'mobx-react';

import qs from 'querystring';

import CenteredPageLayout from '@ui/common/components/CenteredPageLayout';
import NewPasswordForm from '@ui/auth/components/NewPasswordForm';

import { IProps } from './types';

class NewPasswordPage extends PureComponent<IProps> {
  constructor(props) {
    super(props);

    const { location, history, newPasswordFormState } = props;
    const { token } = qs.parse(location.search.substring(1));

    if (token !== undefined) {
      newPasswordFormState.setEmailFromToken(token as string);
    } else {
      history.push('/auth/login');
    }
  }

  public render() {
    return (
      <CenteredPageLayout>
        <NewPasswordForm/>
      </CenteredPageLayout>
    );
  }
}

export default inject('newPasswordFormState')(NewPasswordPage);
