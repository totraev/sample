import React, { SFC } from 'react';

import CenteredPageLayout from '@ui/common/components/CenteredPageLayout';
import RegisterForm from '@ui/auth/components/RegisterForm';

const RegisterPage: SFC = () => (
  <CenteredPageLayout>
    <RegisterForm/>
  </CenteredPageLayout>
);

export default RegisterPage;
