import { FormEventHandler, MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';

import { CodeValue } from '@ui/common/components/TwoFactorCode/types';
import { LoginFormState } from '@ui/auth/states/LoginFormState';
import { AnalyticService } from '@services/AnalyticService';

export interface IOutterProps {
  loginFormState?: LoginFormState;
  analyticService?: AnalyticService;
}

export interface IComponentHandler {
  onChangePassword: FormEventHandler<HTMLInputElement>;
  onChangeEmail: FormEventHandler<HTMLInputElement>;
  onChangeCode: (code: CodeValue) => void;
  onChangeRecaptcha: (value: string) => void;
  onFocusPassword?: FormEventHandler<HTMLInputElement>;
  onFocusEmail?: FormEventHandler<HTMLInputElement>;
  onFocusCode?: (index: number) => void;
  onFormSubmit: MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends
  IOutterProps,
  IComponentHandler,
  InjectedI18nProps,
  InjectedTranslateProps {}
