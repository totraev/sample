import React, { SFC } from 'react';
import { observer, inject } from 'mobx-react';
import { translate } from 'react-i18next';
import './styles.sss';

import { compose, withHandlers, lifecycle } from 'recompose';
import { Link, Redirect } from 'react-router-dom';

import Form from '@ui/common/components/Form';
import TextField from '@ui/common/components/TextField';
import SubmitButton from '@ui/auth/components/SubmitButton';
import TwoFactorCode from '@ui/common/components/TwoFactorCode';
import PasswordField from '@ui/common/components/PasswordField';
import ReCAPTCHA from '@ui/auth/components/ReCaptcha';

import { IProps, IComponentHandler, IOutterProps } from './types';

const LoginForm: SFC<IProps> = ({
  t,
  loginFormState: {
    fields,
    errors,
    serverError,
    twoFaEnabled,
    loading,
    redirect
  },
  onChangeEmail,
  onChangePassword,
  onChangeCode,
  onChangeRecaptcha,
  onFormSubmit,
  onFocusEmail,
  onFocusPassword,
  onFocusCode
}) => (
  <Form
    title={t('login_form.title')}
    error={serverError}
    styleName="wrap"
  >
    <TextField
      label={t('login_form.email')}
      name="email"
      value={fields.email}
      error={errors.email}
      onChange={onChangeEmail}
      onFocus={onFocusEmail}
    />

    <PasswordField
      label={t('login_form.password')}
      name="password"
      value={fields.password}
      error={errors.password}
      onChange={onChangePassword}
      onFocus={onFocusPassword}
    />

    <Link
      styleName="forgot-password"
      to="/auth/reset-password"
    >
      {t('login_form.forgot_password')}
    </Link>

    {twoFaEnabled && (
      <TwoFactorCode
        label={t('login_form.two_factor_code')}
        code={fields.code}
        error={errors.code}
        onChange={onChangeCode}
        onFocus={onFocusCode}
      />
    )}

    <ReCAPTCHA
      value={fields.recaptcha}
      onChange={onChangeRecaptcha}
    />

    <SubmitButton
      disabled={fields.recaptcha === ''}
      loading={loading}
      styleName="submit"
      onClick={onFormSubmit}
      loadingText={t('login_form.loading_text')}
    >
      {t('login_form.submit_button')}
    </SubmitButton>

    {redirect && <Redirect to="/exchange"/>}
  </Form>
);

export default compose<IProps, IOutterProps>(
  inject('loginFormState', 'analyticService'),
  translate('auth'),
  lifecycle<IOutterProps, {}>({
    componentWillUnmount() {
      this.props.loginFormState.resetState();
    }
  }),
  withHandlers<IOutterProps, IComponentHandler>({
    onChangeEmail: ({ loginFormState }) => (e) => {
      loginFormState.updateField('email', e.currentTarget.value);
    },
    onChangePassword: ({ loginFormState }) => (e) => {
      loginFormState.updateField('password', e.currentTarget.value);
    },
    onChangeCode: ({ loginFormState }) => (code) => {
      loginFormState.updateCode(code);
    },
    onChangeRecaptcha: ({ loginFormState, analyticService }) => (value) => {
      analyticService.event('CLICK_LOGIN_RECAPTCHA', 'recaptcha');
      loginFormState.updateRecaptcha(value);
    },
    onFocusEmail: ({ analyticService }) => () => {
      analyticService.event('FOCUS_LOGIN_EMAIL_FIELD', 'focus');
    },
    onFocusPassword: ({ analyticService }) => () => {
      analyticService.event('FOCUS_LOGIN_PASSWORD_FIELD', 'focus');
    },
    onFocusCode: ({ analyticService }) => () => {
      analyticService.event('FOCUS_LOGIN_2FA_CODE_FIELD', 'focus');
    },
    onFormSubmit: ({ loginFormState, analyticService }) => (e) => {
      e.preventDefault();

      analyticService.event('SUBMIT_LOGIN_FORM', 'submit');
      loginFormState.submitForm();
    },
  }),
  observer
)(LoginForm);
