import { FormEventHandler, MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';
import { ResetFormState } from '../../states/ResetFormState';
import { CodeValue } from '@ui/common/components/TwoFactorCode/components/CodeInput/types';

export interface IOutterProps {
  resetFormState?: ResetFormState;
}

export interface IComponentHandler {
  handleUpdateEmail: FormEventHandler<HTMLInputElement>;
  handleUpdateCode: (code: CodeValue) => void;
  handleRecaptchaChange: (value: string) => void;
  handleSubmit: MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends
  IOutterProps,
  IComponentHandler,
  InjectedI18nProps,
  InjectedTranslateProps {}
