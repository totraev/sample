import React, { SFC } from 'react';
import { inject, observer } from 'mobx-react';
import './styles.sss';

import { compose, withHandlers, lifecycle } from 'recompose';
import { translate, Trans } from 'react-i18next';

import Form from '@ui/common/components/Form';
import TextField from '@ui/common/components/TextField';
import SubmitButton from '@ui/auth/components/SubmitButton';
import TwoFactorCode from '@ui/common/components/TwoFactorCode';
import ReCAPTCHA from '@ui/auth/components/ReCaptcha';

import { IProps, IComponentHandler, IOutterProps } from './types';

const ResetPasswordForm: SFC<IProps> = ({
  resetFormState: {
    fields,
    errors,
    serverError,
    success,
    twoFaEnabled,
    loading
  },
  t,
  handleUpdateEmail,
  handleUpdateCode,
  handleRecaptchaChange,
  handleSubmit
}) => {
  const info = (
    <Trans
      i18nKey="reset_password_form.form_info_message"
      components={[<br key="0"/>]}
    />
  );

  return (
    <Form
      title={t('reset_password_form.title')}
      error={serverError}
      info={success ? info : ''}
      styleName="wrap"
    >
      <TextField
        label={t('reset_password_form.email')}
        name="email"
        value={fields.email}
        onChange={handleUpdateEmail}
        disabled={success}
        hint={!success ? t('reset_password_form.email_hint') : ''}
        error={errors.email}
      />

      {twoFaEnabled && (
        <TwoFactorCode
          label={t('reset_password_form.two_factor_code')}
          disabled={success}
          code={fields.code}
          error={errors.code}
          onChange={handleUpdateCode}
        />
      )}

      <ReCAPTCHA
        value={fields.recaptcha}
        onChange={handleRecaptchaChange}
      />

      <SubmitButton
        loading={loading}
        styleName="submit"
        onClick={handleSubmit}
        disabled={success || fields.recaptcha === ''}
        loadingText={t('reset_password_form.loading_text')}
      >
        {t('reset_password_form.submit_button')}
      </SubmitButton>
    </Form>
  );
};

export default compose<IProps, IOutterProps>(
  inject('resetFormState'),
  translate('auth'),
  lifecycle<IOutterProps, {}>({
    componentWillUnmount() {
      this.props.resetFormState.resetState();
    }
  }),
  withHandlers<IOutterProps, IComponentHandler>({
    handleUpdateEmail: ({ resetFormState }) => (e) => {
      resetFormState.updateField('email', e.currentTarget.value);
    },
    handleUpdateCode: ({ resetFormState }) => (code) => {
      resetFormState.updateCode(code);
    },
    handleRecaptchaChange: ({ resetFormState }) => (value) => {
      resetFormState.updateRecaptcha(value);
    },
    handleSubmit: ({ resetFormState }) => (e) => {
      e.preventDefault();
      resetFormState.submitForm();
    },
  }),
  observer
)(ResetPasswordForm);
