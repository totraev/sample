import React, { SFC } from 'react';
import './styles.sss';

import { observer, inject } from 'mobx-react';
import { compose, withHandlers, lifecycle } from 'recompose';
import { translate, Trans } from 'react-i18next';
import { Link, Redirect } from 'react-router-dom';

import Form from '@ui/common/components/Form';
import TextField from '@ui/common/components/TextField';
import SubmitButton from '@ui/auth/components/SubmitButton';
import PasswordField from '@ui/common/components/PasswordField';
import ReCAPTCHA from '@ui/auth/components/ReCaptcha';

import { IProps, IComponentHandler, IOutterProps } from './types';

const RegisterForm: SFC<IProps> = ({
  t,
  registerFormState: {
    fields,
    errors,
    serverError,
    loading,
    redirect
  },
  onChangeEmail,
  onChangePassword,
  onChangeRepeatPassword,
  onChangeRecaptcha,
  onSubmit,
  onFocusEmail,
  onFocusPassword,
  onFocusRepeatPassword
}) => (
  <Form
    title={t('register_form.title')}
    error={serverError}
    styleName="wrap"
  >
    <TextField
      label={t('register_form.email')}
      name="email"
      value={fields.email}
      error={errors.email}
      onChange={onChangeEmail}
      onFocus={onFocusEmail}
    />

    <PasswordField
      label={t('register_form.password')}
      name="password"
      value={fields.password}
      error={errors.password}
      onChange={onChangePassword}
      onFocus={onFocusPassword}
    />

    <PasswordField
      label={t('register_form.confirm_password')}
      name="repeat_password"
      value={fields.repeatPassword}
      error={errors.repeatPassword}
      onChange={onChangeRepeatPassword}
      onFocus={onFocusRepeatPassword}
    />

    <ReCAPTCHA
      value={fields.recaptcha}
      onChange={onChangeRecaptcha}
    />

    <SubmitButton
      disabled={fields.recaptcha === ''}
      loading={loading}
      styleName="submit"
      onClick={onSubmit}
      loadingText={t('register_form.loading_text')}
    >
      {t('register_form.submit_button')}
    </SubmitButton>

    <div styleName="link-block">
      <Trans
        i18nKey="register_form.terms"
        components={[
          <Link
            key="0"
            styleName="link"
            to="/terms-and-private-policy"
            children="Terms & Private Polic"
          />
        ]}
      />
    </div>

    {redirect && <Redirect to="/exchange"/>}
  </Form>
);

export default compose<IProps, IOutterProps>(
  inject('registerFormState', 'analyticService'),
  translate('auth'),
  lifecycle<IProps, {}>({
    componentWillUnmount() {
      this.props.registerFormState.resetState();
    }
  }),
  withHandlers<IOutterProps, IComponentHandler>({
    onChangeEmail: ({ registerFormState }) => (e) => {
      registerFormState.updateField('email', e.currentTarget.value);
    },
    onChangePassword: ({ registerFormState }) => (e) => {
      registerFormState.updateField('password', e.currentTarget.value);
    },
    onChangeRepeatPassword: ({ registerFormState }) => (e) => {
      registerFormState.updateField('repeatPassword', e.currentTarget.value);
    },
    onFocusEmail: ({ analyticService }) => () => {
      analyticService.event('FOCUS_REG_EMAIL_FIELD', 'focus');
    },
    onFocusPassword: ({ analyticService }) => () => {
      analyticService.event('FOCUS_REG_PASS_FIELD', 'focus');
    },
    onFocusRepeatPassword: ({ analyticService }) => () => {
      analyticService.event('FOCUS_REG_REPEAT_PASS_FIELDS', 'focus');
    },
    onChangeRecaptcha: ({ registerFormState, analyticService }) => (value) => {
      analyticService.event('CLICK_REG_RECAPTCHA', 'recaptcha');
      registerFormState.updateRecaptcha(value);
    },
    onSubmit: ({ registerFormState, analyticService }) => (e) => {
      e.preventDefault();

      analyticService.event('SUBMIT_REG_FORM', 'submit');
      registerFormState.submitForm();
    }
  }),
  observer
)(RegisterForm);
