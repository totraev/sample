import { FormEventHandler, MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';

import { RegisterFormState } from '@ui/auth/states/RegisterFormState';
import { AnalyticService } from '@services/AnalyticService';

export interface IOutterProps {
  registerFormState?: RegisterFormState;
  analyticService?: AnalyticService;
}

export interface IComponentHandler {
  onChangeEmail: FormEventHandler<HTMLInputElement>;
  onChangePassword: FormEventHandler<HTMLInputElement>;
  onChangeRepeatPassword: FormEventHandler<HTMLInputElement>;
  onChangeRecaptcha: (value: string) => void;
  onFocusEmail?: FormEventHandler<HTMLInputElement>;
  onFocusPassword?: FormEventHandler<HTMLInputElement>;
  onFocusRepeatPassword?: FormEventHandler<HTMLInputElement>;
  onSubmit: MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends
  IOutterProps,
  IComponentHandler,
  InjectedI18nProps,
  InjectedTranslateProps {}
