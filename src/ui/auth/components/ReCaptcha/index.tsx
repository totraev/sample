import React, { PureComponent, createRef } from 'react';
import GReCAPTCHA from 'react-google-recaptcha';
import './styles.sss';

import config from '../../../../config';
import { IProps } from './types';

class ReCAPTCHA extends PureComponent<IProps> {
  private recaptchaRef = createRef<GReCAPTCHA>();

  public componentDidUpdate(): void {
    if (this.props.value === '' && this.recaptchaRef.current.getValue() !== '') {
      this.recaptchaRef.current.reset();
    }
  }

  public render() {
    const { onChange } = this.props;

    return (
      <div styleName="recaptcha">
        <GReCAPTCHA
          ref={this.recaptchaRef}
          sitekey={config.recaptchaSiteKey}
          theme="dark"
          size="normal"
          onChange={onChange}
        />
      </div>
    );
  }
}

export default ReCAPTCHA;
