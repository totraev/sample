import React, { SFC } from 'react';
import { inject, observer } from 'mobx-react';
import { translate } from 'react-i18next';
import './styles.sss';

import { compose, lifecycle, withHandlers } from 'recompose';

import Form from '@ui/common/components/Form';
import TextField from '@ui/common/components/TextField';
import SubmitButton from '@ui/auth/components/SubmitButton';
import TwoFactorCode from '@ui/common/components/TwoFactorCode';
import PasswordField from '@ui/common/components/PasswordField';

import { IProps, IComponentHandler, IOutterProps } from './types';
import { Redirect } from 'react-router';

const NewPasswordForm: SFC<IProps> = ({
  t,
  newPasswordFormState,
  handleUpdateEmail,
  handleUpdatePassword,
  handleUpdateRepeatPassword,
  handleUpdateCode,
  handleSubmit
}) => (
  <Form
    title={t('new_password_form.title')}
    error={newPasswordFormState.serverError}
    styleName="wrap"
  >
    <TextField
      label={t('new_password_form.email')}
      name="email"
      value={newPasswordFormState.fields.email}
      onChange={handleUpdateEmail}
      disabled={Boolean(newPasswordFormState.fields.email)}
    />

    <PasswordField
      label={t('new_password_form.password')}
      name="password"
      value={newPasswordFormState.fields.password}
      error={newPasswordFormState.errors.password}
      onChange={handleUpdatePassword}
    />

    <PasswordField
      label={t('new_password_form.confirm_password')}
      name="repeat_password"
      value={newPasswordFormState.fields.repeatPassword}
      error={newPasswordFormState.errors.repeatPassword}
      onChange={handleUpdateRepeatPassword}
    />

    {newPasswordFormState.twoFaEnabled && (
      <TwoFactorCode
        label={t('new_password_form.two_factor_code')}
        code={newPasswordFormState.fields.code}
        error={newPasswordFormState.errors.code}
        onChange={handleUpdateCode}
      />
    )}

    <SubmitButton
      loading={newPasswordFormState.loading}
      styleName="submit"
      onClick={handleSubmit}
      loadingText={t('new_password_form.loading_text')}
    >
      {t('new_password_form.submit_button')}
    </SubmitButton>

    {newPasswordFormState.success && <Redirect to="/auth/login"/>}
  </Form>
);

const withComponentHandlers = withHandlers<IOutterProps, IComponentHandler>({
  handleUpdateEmail: ({ newPasswordFormState }) => (e) => {
    newPasswordFormState.updateField('email', e.currentTarget.value);
  },
  handleUpdatePassword: ({ newPasswordFormState }) => (e) => {
    newPasswordFormState.updateField('password', e.currentTarget.value);
  },
  handleUpdateRepeatPassword: ({ newPasswordFormState }) => (e) => {
    newPasswordFormState.updateField('repeatPassword', e.currentTarget.value);
  },
  handleUpdateCode: ({ newPasswordFormState }) => (code) => {
    newPasswordFormState.updateCode(code);
  },
  handleSubmit: ({ newPasswordFormState }) => (e) => {
    e.preventDefault();
    newPasswordFormState.submitForm();
  }
});

const withComponentLifecycles = lifecycle<IProps, {}>({
  componentWillUnmount() {
    this.props.newPasswordFormState.resetState();
  }
});

export default compose<IProps, IOutterProps>(
  inject('newPasswordFormState'),
  translate('auth'),
  withComponentHandlers,
  withComponentLifecycles,
  observer
)(NewPasswordForm);
