import { FormEventHandler, MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';
import { NewPasswordFormState } from '../../states/NewPasswordFormState';
import { CodeValue } from '@ui/common/components/TwoFactorCode/components/CodeInput/types';

export interface IOutterProps {
  newPasswordFormState?: NewPasswordFormState;
}

export interface IComponentHandler {
  handleUpdateEmail: FormEventHandler<HTMLInputElement>;
  handleUpdatePassword: FormEventHandler<HTMLInputElement>;
  handleUpdateRepeatPassword: FormEventHandler<HTMLInputElement>;
  handleUpdateCode: (value: CodeValue) => void;
  handleSubmit: MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends
  IOutterProps,
  IComponentHandler,
  InjectedI18nProps,
  InjectedTranslateProps {}
