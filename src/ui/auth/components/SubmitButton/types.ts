import { IProps as IButtonProps } from '@ui/common/components/Button/types';

export interface IProps extends IButtonProps {
  loading?: boolean;
  loadingText?: string;
}
