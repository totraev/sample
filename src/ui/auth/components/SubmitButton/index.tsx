import React, { SFC } from 'react';

import Button from '../../../common/components/Button';
import { IProps } from './types';

const SubmitButton: SFC<IProps> = ({
  children,
  loading,
  loadingText,
  disabled,
  ...buttonProps
}) => (
  <Button disabled={loading || disabled} {...buttonProps}>
    {loading ? loadingText : children}
  </Button>
);

SubmitButton.defaultProps = {
  loading: false,
  loadingText: 'Loading...'
};

export default SubmitButton;
