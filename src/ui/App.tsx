import React from 'react';
import { inject } from 'mobx-react';

import { Helmet } from 'react-helmet';
import { compose } from 'recompose';
import { Switch, Route, Redirect, withRouter, RouteComponentProps } from 'react-router-dom';
import { UnregisterCallback } from 'history';
import { hot } from 'react-hot-loader';
import Loadable from 'react-loadable';
import { translate, InjectedTranslateProps } from 'react-i18next';

import Notifications from '@ui/common/components/Notifications';
import AppLayout from '@ui/common/components/AppLayout';
import DocumentationPage from '@ui/common/pages/DocumentationPage';
import TermsServicesPage from '@ui/common/pages/TermsServicesPage';
import PageLoader from '@ui/common/components/PageLoader';

import { AnalyticService } from '@services/AnalyticService';
import { RootStore } from '@stores/rootStore';

const ExchangePage = Loadable({
  loader: () => import(
    /* webpackChunkName: "exchange-page" */
    './exchange/pages/ExchangePage'
  ),
  loading: () => <PageLoader/>
});

const AuthPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "auth-page" */
    './auth/pages/AuthPage'
  ),
  loading: () => <PageLoader/>
});

const UserPage = Loadable({
  loader: () => import(
    /* webpackChunkName: "user-page" */
    './user/pages/UserPage'
  ),
  loading: () => <PageLoader/>
});

export interface IProps extends RouteComponentProps<{}>, InjectedTranslateProps {
  analyticService: AnalyticService;
  rootStore: RootStore;
}

class App extends React.Component<IProps> {
  private unlisten: UnregisterCallback = null;

  public componentDidMount(): void {
    window.addEventListener('load', this.onPageLoad);
  }

  public componentWillUnmount(): void {
    window.removeEventListener('load', this.onPageLoad);
    this.unlisten();
  }

  private onPageLoad = () => {
    const { analyticService, rootStore, history } = this.props;

    this.unlisten = history.listen((location) => {
      const fullPath = `${location.pathname}${location.search}${location.hash}`;

      analyticService.pageChange(fullPath);
    });

    if (rootStore.auth.isAuth) {
      analyticService.setUser(rootStore.auth.authData.userId);
    }
  }

  render() {
    const { t } = this.props;

    return (
      <AppLayout>
        <Helmet>
          <meta charSet="utf-8" />
          <title>{t('app.title')}</title>
        </Helmet>

        <Switch>
          <Route path="/exchange" component={ExchangePage}/>
          <Route path="/auth" component={AuthPage}/>
          <Route path="/user" component={UserPage}/>
          <Route exect path="/documentation" component={DocumentationPage}/>
          <Route exect path="/terms-and-private-policy" component={TermsServicesPage}/>

          <Redirect exact from="/" to="/exchange"/>
        </Switch>

        <Notifications/>
      </AppLayout>
    );
  }
}

export default compose<IProps, {}>(
  hot(module),
  inject('analyticService', 'rootStore'),
  translate('common'),
  withRouter
)(App);
