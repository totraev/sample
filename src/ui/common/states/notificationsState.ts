import { computed } from 'mobx';

import rootStore, { RootStore } from '@stores/rootStore';
import NotificationModel from '@models/NotificationModel';

export class NotificationsState {
  constructor(private rootStore: RootStore) {}

  @computed public get notifications(): NotificationModel[] {
    return this.rootStore.notifications.notifications;
  }
}

export default new NotificationsState(rootStore);
