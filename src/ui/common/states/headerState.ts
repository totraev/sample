import { computed, observable, action } from 'mobx';

import rootStore, { RootStore } from '@stores/rootStore';
import BalanceModel from '@models/BalanceModel';

export class HeaderState {
  @observable public sidebarOpen: boolean = false;

  constructor(public rootStore: RootStore) {}

  @computed public get currentBalance(): BalanceModel {
    return !this.loading
      ? this.rootStore.balances.balancesMap.get('BTC')
      : new BalanceModel({
        curr: '',
        balance: '0',
        blocked: '0',
        upnl: '0'
      });
  }

  @computed public get loading(): boolean {
    return this.rootStore.balances.loading;
  }

  @computed public get isAuth(): boolean {
    return this.rootStore.auth.isAuth;
  }

  @action public closeSidebar = () => {
    this.sidebarOpen = false;
  }

  @action public openSidebar = () => {
    this.sidebarOpen = true;
  }
}

export default new HeaderState(rootStore);
