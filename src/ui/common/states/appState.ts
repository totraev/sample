import { observable, computed, autorun, action } from 'mobx';

import rootStore, { RootStore } from '@stores/rootStore';

export class AppState {
  @observable public status: 'connected' | 'disconnected' | 'connecting';

  constructor(private rootStore: RootStore) {
    autorun(() => {
      switch (this.status) {
        case 'disconnected':
          return this.rootStore.notifications.addErrorNotification('Connection lost');
        case 'connected':
          return this.rootStore.notifications.addSuccessNotification('Connection established');
        case 'connecting':
          return this.rootStore.notifications.addInfoNotification('Connecting...');
      }
    });
  }

  @computed public get connected(): boolean {
    return this.status === 'connected';
  }

  @action public setStatus(status: 'connected' | 'disconnected' | 'connecting'): void {
    this.status = status;
  }
}

export default new AppState(rootStore);
