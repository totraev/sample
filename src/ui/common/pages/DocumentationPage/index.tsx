import React, { SFC } from 'react';

import CenteredPageLayout from '@ui/common/components/CenteredPageLayout';
import Stub from '@ui/common/components/Stub';

const DocumentationPage: SFC = () => (
  <CenteredPageLayout>
    <Stub
      stubTitle="Coming soon"
      message="We're working hard to get this section done."
    />
  </CenteredPageLayout>
);

export default DocumentationPage;
