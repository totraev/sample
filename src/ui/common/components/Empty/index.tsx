import React, { SFC } from 'react';
import './styles.sss';

const Empty: SFC = ({ children }) => (
  <div styleName="empty">{children}</div>
);

export default Empty;
