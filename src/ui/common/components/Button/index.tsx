import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Button: SFC<IProps> = ({ buttonType, className, ...buttonProps }) => (
  <button
    styleName={buttonType}
    className={className}
    {...buttonProps}
  />
);

Button.defaultProps = { buttonType: 'button' };

export default Button;
