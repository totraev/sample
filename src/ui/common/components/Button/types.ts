import { HTMLProps } from 'react';

export interface IProps extends HTMLProps<HTMLButtonElement>{
  buttonType?: 'button' | 'outline' | 'link';
}
