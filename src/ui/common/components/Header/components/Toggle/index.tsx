import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Toggle: SFC<IProps> = ({ className, ...props }) => (
  <button styleName="toggle" className={className} {...props}/>
);

export default Toggle;
