import React, { SFC } from 'react';
import './styles.sss';

const css = require('./styles.sss');

import { NavLink } from 'react-router-dom';
import { IProps } from './types';

const NavItem: SFC<IProps> = ({ className, ...props }) => (
  <NavLink
    styleName="nav-item"
    className={className}
    activeClassName={css['nav-item--active']}
    {...props}
  />
);

export default NavItem;
