import { MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';

import AuthStore from '@stores/authStore';
import { HeaderState } from '@ui/common/states/headerState';

export interface IOutterProps {
  authStore: AuthStore;
  headerState: HeaderState;
  onLogOut: MouseEventHandler<HTMLAnchorElement>;
}

export interface IProps extends IOutterProps, InjectedI18nProps, InjectedTranslateProps {}
