import React, { SFC } from 'react';
import './styles.sss';
const css = require('./styles.sss');

import { observer } from 'mobx-react';
import { translate } from 'react-i18next';
import { compose } from 'recompose';
import classNames from 'classnames';
import { Link, NavLink } from 'react-router-dom';

import { IProps, IOutterProps } from './types';
import config from '../../../../../../config';

import Logo from '../Logo';

const Sidebar: SFC<IProps> = ({ authStore, headerState, onLogOut, t }) => {
  const { currentBalance } = headerState;
  const upnlStyleName = classNames('balance-value', {
    'balance-value--up': currentBalance.upnl.gt(0),
    'balance-value--down': currentBalance.upnl.lt(0),
  });

  return (
    <aside styleName={headerState.sidebarOpen ? 'sidebar--open' : 'sidebar'}>
      <Link to="/">
        <Logo styleName="logo"/>
      </Link>

      <button
        styleName="close"
        onClick={headerState.closeSidebar}
      />

      <nav styleName="trade-nav" onClick={headerState.closeSidebar}>
        <NavLink
          to="/exchange"
          styleName="nav-item"
          activeClassName={css['nav-item--active']}
        >
          {t('header.trade')}
        </NavLink>
        <a
          styleName="nav-item"
          href={config.specUrl}
        >
          {t('header.docs')}
        </a>
        <NavLink
          to="/user/api"
          styleName="nav-item"
          activeClassName={css['nav-item--active']}
        >
          {t('header.api')}
        </NavLink>
      </nav>

      {headerState.isAuth && <>
        <nav styleName="balance-nav" onClick={headerState.closeSidebar}>
          <NavLink
            to="/user/deposit"
            styleName="nav-item"
            activeClassName={css['nav-item--active']}
          >
            {t('header.deposit')}
          </NavLink>
          <NavLink
            to="/user/withdrawal"
            styleName="nav-item"
            activeClassName={css['nav-item--active']}
          >
            {t('header.withdraw')}
          </NavLink>
        </nav>

        <div styleName="balance">
          <div styleName="balance-item">
            <span styleName="balance-label">{t('header.balance')}</span>
            <span styleName="balance-value">
              {currentBalance.balance.toFixed(8)}
              &nbsp;
              <span styleName="curr">{currentBalance.currency}</span>
            </span>
          </div>
          <div styleName="balance-item">
            <span styleName="balance-label">{t('header.available')}</span>
            <span styleName="balance-value">
              {currentBalance.available.toFixed(8)}
              &nbsp;
              <span styleName="curr">{currentBalance.currency}</span>
            </span>
          </div>
          <div styleName="balance-item">
            <span styleName="balance-label">{t('header.used')}</span>
            <span styleName="balance-value">
              {currentBalance.used.toFixed(8)}
              &nbsp;
              <span styleName="curr">{currentBalance.currency}</span>
            </span>
          </div>
          <div styleName="balance-item">
            <span styleName="balance-label">{t('header.pnl')}</span>
            <span styleName={upnlStyleName}>
              {currentBalance.upnl.toFixed(8)}
              &nbsp;
              <span styleName="curr">{currentBalance.currency}</span>
            </span>
          </div>
        </div>
      </>}

      {headerState.isAuth
        ? (
          <nav styleName="auth-nav" onClick={headerState.closeSidebar}>
            <NavLink to="/user/settings" styleName="nav-item">
              {t('header.settings')}
            </NavLink>
            <NavLink to="#" styleName="nav-item" onClick={onLogOut}>
              {t('header.log_out')}
            </NavLink>
          </nav>
        )
        : (
          <nav styleName="auth-nav" onClick={headerState.closeSidebar}>
            <NavLink to="/auth/login" styleName="nav-item">
              {t('header.log_in')}
            </NavLink>
            <NavLink to="/auth/register" styleName="nav-item">
              {t('header.register')}
            </NavLink>
          </nav>
        )
      }

      <div styleName="email">{authStore.authData.email}</div>
    </aside>
  );
};

export default compose<IProps, IOutterProps>(
  translate('common'),
  observer
)(Sidebar);
