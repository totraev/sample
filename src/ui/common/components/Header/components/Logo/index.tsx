import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Logo: SFC<IProps> = ({ className }) => (
  <div
    styleName="logo"
    className={className}
  />
);

export default Logo;
