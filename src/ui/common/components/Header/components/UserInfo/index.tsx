import React, { SFC } from 'react';
import { translate } from 'react-i18next';

import NavItem from '../NavItem';

import { IProps } from './types';
import './styles.sss';

const UserInfo: SFC<IProps> = ({ className, email, onLogOut, t }) => (
  <div styleName="info" className={className}>
    <div styleName="email">{email}</div>

    <div styleName="nav">
      <NavItem styleName="nav-item" to="/user/settings">{t('header.settings')}</NavItem>
      <NavItem styleName="nav-item" to="#" onClick={onLogOut}>{t('header.log_out')}</NavItem>
    </div>
  </div>
);

export default translate('common')(UserInfo);
