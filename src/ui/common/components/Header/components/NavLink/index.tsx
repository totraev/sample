import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const NavItem: SFC<IProps> = ({ className, ...props }) => (
  <a
    styleName="nav-item"
    className={className}
    {...props}
  />
);

export default NavItem;
