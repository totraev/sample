import React, { SFC } from 'react';
import { observer } from 'mobx-react';

import { compose } from 'recompose';
import { translate } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import UnrealizedPnL from '../UnrealizedPnL';
import NavItem from '../NavItem';
import BalanceModel from '@models/BalanceModel';

import { IProps, IOutterProps } from './types';
import './styles.sss';

const BalanceInfo: SFC<IProps> = ({ balance, t }) => (
  <div styleName="wallet">
    <div styleName="wallet-balance">
      <div styleName="balance">
        <span styleName="label">{t('header.balance')}</span>
        &nbsp;
        <span styleName="value">{balance.balance.toFixed(8)}</span>
        &nbsp;
        <span styleName="curr">{balance.currency}</span>
      </div>

      <nav styleName="nav">
        <NavItem styleName="nav-item" to="/user/deposit">{t('header.deposit')}</NavItem>
        <NavItem styleName="nav-item" to="/user/withdrawal">{t('header.withdraw')}</NavItem>
      </nav>
    </div>

    <div styleName="block">
      <div styleName="labels">
        <span styleName="label">{t('header.available')}</span>
        <span styleName="label">{t('header.used')}</span>
      </div>

      <div styleName="values">
        <span styleName="value">
          &nbsp;
          {balance.available.toFixed(8)}
          &nbsp;
          <span styleName="curr">{balance.currency}</span>
        </span>

        <span styleName="value">
          &nbsp;
          {balance.used.toFixed(8)}
          &nbsp;
          <span styleName="curr">{balance.currency}</span>
        </span>
      </div>
    </div>

    <UnrealizedPnL
      label={t('header.pnl')}
      value={balance.upnl}
      currency={balance.currency}
    />
  </div>
);

BalanceInfo.defaultProps = {
  balance: new BalanceModel({
    curr: '',
    balance: '0',
    blocked: '0',
    upnl: '0'
  })
};

export default compose<IProps, IOutterProps>(
  translate('common'),
  withRouter,
  observer
)(BalanceInfo);
