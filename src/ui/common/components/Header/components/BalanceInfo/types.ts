import { InjectedTranslateProps, InjectedI18nProps } from 'react-i18next';
import BalanceModel from '@models/BalanceModel';

export interface IOutterProps {
  balance: BalanceModel;
}

export interface IProps extends IOutterProps, InjectedTranslateProps, InjectedI18nProps {}
