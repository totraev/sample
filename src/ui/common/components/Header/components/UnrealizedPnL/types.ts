import BigNumber from 'bignumber.js';

export interface IProps {
  label: string;
  value: BigNumber;
  currency: string;
}
