import React, { SFC } from 'react';
import './styles.sss';

import classNames from 'classnames';
import { IProps } from './types';

const UnrealizedPnL: SFC<IProps> = ({ label, value, currency }) => {
  const styleName = classNames({
    stats: value.isZero(),
    'stats--up': value.gt(0),
    'stats--down': value.lt(0)
  });

  return (
    <div styleName="pnl">
      <div styleName="label">{label}</div>

      <div styleName={styleName}>
        {value.toFixed(8)}
        &nbsp;
        <span styleName="curr">{currency}</span>
      </div>
    </div>
  );
};

export default UnrealizedPnL;
