import React, { SFC } from 'react';
import { inject, observer } from 'mobx-react';
import { translate } from 'react-i18next';

import { Link, withRouter } from 'react-router-dom';
import { compose, withHandlers } from 'recompose';

import Logo from './components/Logo';
import Toggle from './components/Toggle';
import NavItem from './components/NavItem';
import NavLink from './components/NavLink';
import BalanceInfo from './components/BalanceInfo';
import UserInfo from './components/UserInfo';
import Sidebar from './components/Sidebar';

import { IProps, IOutterProps, IHandlerProps, IHandlers } from './types';
import config from '../../../../config';

import './styles.sss';

const Header: SFC<IProps> = ({ rootStore, headerState, onLogOut, t }) => (
  <header styleName="header">
    <Link to="/"><Logo /></Link>

    <div styleName="header-nav">
      <NavItem styleName="nav-item" to="/exchange">{t('header.trade')}</NavItem>
      <NavLink styleName="nav-item" href={config.specUrl}>{t('header.docs')}</NavLink>
      {rootStore.auth.isAuth &&
        <NavItem styleName="nav-item" to="/user/api">
        {t('header.api')}
        </NavItem>
      }
    </div>

    {headerState.isAuth ? <>
      <BalanceInfo
        balance={headerState.currentBalance}
      />

      <UserInfo
        email={rootStore.auth.authData.email}
        onLogOut={onLogOut}
      />
    </> : (
      <div styleName="header-auth">
        <NavItem
          styleName="login-link"
          to="/auth/login"
        >
          {t('header.log_in')}
        </NavItem>
        <NavItem
          styleName="register-link"
          to="/auth/register"
        >
          {t('header.register')}
        </NavItem>
      </div>
    )}

    <div styleName={headerState.isAuth ? 'header-toggle--isAuth' : 'header-toggle'}>
      <Toggle onClick={headerState.openSidebar}/>
    </div>

    <Sidebar
      authStore={rootStore.auth}
      headerState={headerState}
      onLogOut={onLogOut}
    />
  </header>
);

BalanceInfo.defaultProps = {
  balance: null
};

export default compose<IProps, IOutterProps>(
  inject('rootStore', 'headerState'),
  translate('common'),
  withRouter,
  withHandlers<IHandlerProps, IHandlers>({
    onLogOut: ({ rootStore, history }) => async (e) => {
      e.preventDefault();

      try {
        await rootStore.auth.logout();
      } catch (e) {
        rootStore.auth.reset();
        rootStore.notifications.addErrorNotification(e.message);
      } finally {
        history.push('/auth/login');
      }
    }
  }),
  observer
)(Header);
