import { MouseEventHandler } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router-dom';

import { HeaderState } from '@ui/common/states/headerState';
import { RootStore } from '@stores/rootStore';

export interface IOutterProps {
  rootStore?: RootStore;
  headerState?: HeaderState;
}

export interface IHandlerProps extends IOutterProps, RouteComponentProps<void> {}

export interface IHandlers {
  onLogOut: MouseEventHandler<HTMLAnchorElement>;
}

export interface IProps extends
  IHandlerProps,
  IHandlers,
  InjectedI18nProps,
  InjectedTranslateProps {}
