import { HTMLProps } from 'react';

export interface IProps extends HTMLProps<HTMLTableElement> {}
