import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Table: SFC<IProps> = props => (
  <table
    styleName="table"
    className={props.className}
    {...props}
  />
);

export default Table;
