import React, { SFC } from 'react';
import './styles.sss';

import { withHandlers } from 'recompose';

import { IProps, IOutterProps, IHandlers } from './types';

const Option: SFC<IProps> = ({ children, focused, handleOnClick, onHover, optionRef }) => {
  return (
    <div
      ref={optionRef}
      styleName={focused ? 'option--focused' : 'option'}
      onClick={handleOnClick}
      onMouseEnter={onHover}
    >
      {children}
    </div>
  );
};

export default withHandlers<IOutterProps, IHandlers>({
  handleOnClick: ({ value, children, onClick }) => (e) => {
    e.stopPropagation();

    onClick({ value, name: children });
  }
})(Option);
