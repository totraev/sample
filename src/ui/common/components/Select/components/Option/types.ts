import { MouseEventHandler, Ref } from 'react';

export interface IOption {
  value: string;
  name: string;
}

export interface IOutterProps {
  value: string;
  focused: boolean;
  children: string;
  onClick: (option: IOption) => void;
  onHover: () => void;
  optionRef?: Ref<HTMLDivElement>;
}

export interface IHandlers {
  handleOnClick: MouseEventHandler<HTMLDivElement>;
}

export interface IProps extends IOutterProps, IHandlers {}
