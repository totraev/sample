export interface IOption {
  value: string;
  name: string;
}

export interface IProps {
  options?: IOption[];
  disabled?: boolean;
  onChange?: (arg: IOption) => void;
  onFocus?: () => void;
  onBlur?: () => void;
  value?: IOption | string;
  placeholder?: string;
}

export interface IState {
  open: boolean;
  focus: boolean;
  selectedOption: IOption;
  focused: number;
  options: IOption[];
}
