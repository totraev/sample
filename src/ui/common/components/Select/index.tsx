import React, { Component, KeyboardEventHandler, createRef } from 'react';
import './styles.sss';

import Scrollbars from 'react-custom-scrollbars';
import CustomScrollbar from '@ui/common/components/CustomScrollbar';
import Option from './components/Option';

import { IProps, IState, IOption } from './types';
import { ARROW_UP, ARROW_DOWN, ENTER } from './constants';

class Select extends Component<IProps, IState> {
  private wrapperRef = createRef<HTMLDivElement>();
  private menuRef = createRef<Scrollbars>();
  private options: HTMLDivElement[] = [];

  public static defaultProps = {
    options: [],
    onChange: () => {},
    onFocus: () => {},
    onBlur: () => {},
    value: { value: '', name: '' }
  };

  public state: IState = {
    open: false,
    focus: false,
    selectedOption: { value: '', name: '' },
    options: [],
    focused: null
  };

  public componentDidUpdate(): void {
    const { open, focused } = this.state;

    if (open && focused !== null) {
      this.scrollIntoView(this.options[focused]);
    }
  }

  public static getDerivedStateFromProps(props: IProps, state: IState): Partial<IState> {
    const selectedOption = typeof props.value === 'string'
      ? props.options.find(option => option.value === props.value)
      : props.value;

    return {
      ...state,
      options: props.options !== state.options ? props.options : state.options,
      selectedOption: Boolean(selectedOption) && selectedOption !== state.selectedOption
        ? selectedOption
        : state.selectedOption
    };
  }

  private handleFocus = () => {
    this.setState({
      focus: true,
      open: true
    });

    this.props.onFocus();
  }

  private handleBlur = () => {
    this.setState({
      focus: false,
      open: false,
      focused: null
    });

    this.wrapperRef.current.blur();
    this.props.onBlur();
  }

  private handleKeyDown: KeyboardEventHandler<HTMLDivElement> = (e) => {
    const { open, focused, options } = this.state;

    if (!open) {
      return;
    }

    switch (e.keyCode) {
      case ARROW_UP:
        this.setState({
          focused: focused === null || focused === 0 ? options.length - 1 : focused - 1
        });
        break;
      case ARROW_DOWN:
        this.setState({
          focused: focused === null || focused === options.length - 1 ? 0 : focused + 1
        });
        break;
      case ENTER:
        this.handleOptionClick(options[focused]);
        break;
    }
  }

  private handleOptionHover = (focused: number) => () =>  {
    this.setState({ focused });
  }

  private handleOptionClick = (selectedOption: IOption) => {
    this.setState({
      selectedOption,
      open: false,
      focus: false,
      focused: null
    });

    this.wrapperRef.current.blur();
    this.props.onChange(selectedOption);
  }

  private scrollIntoView(option: HTMLDivElement): void {
    const { clientHeight, scrollTop } = this.menuRef.current.getValues();
    const overScroll = option.offsetHeight / 3;

    if (option.offsetTop + overScroll > clientHeight + scrollTop) {
      this.menuRef.current.scrollTop(option.offsetTop + option.offsetHeight - clientHeight);
    } else if (option.offsetTop + overScroll < scrollTop) {
      this.menuRef.current.scrollTop(option.offsetTop);
    }
  }

  private optionRef = (index: number) => (element: HTMLDivElement) => {
    this.options[index] = element;
  }

  public render() {
    const { placeholder } = this.props;
    const { open, focus, selectedOption, options, focused } = this.state;

    return (
      <div
        tabIndex={0}
        ref={this.wrapperRef}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onClick={this.handleFocus}
        onKeyDown={this.handleKeyDown}
        styleName={focus ? 'select--focused' : 'select'}
      >
        <div styleName="arrow-wrap">
          <div styleName="value">
            {Boolean(selectedOption.name) ? selectedOption.name : placeholder}
          </div>

          {open
            ? <span styleName="arrow--up"/>
            : <span styleName="arrow--down"/>}
        </div>

        {open && (
          <div styleName="menu">
            <CustomScrollbar scrollbarRef={this.menuRef} autoHeight>
              {options.map(({ name, value }, i) => (
                <Option
                  optionRef={this.optionRef(i)}
                  key={value}
                  focused={focused === i}
                  value={value}
                  onClick={this.handleOptionClick}
                  onHover={this.handleOptionHover(i)}
                >
                  {name}
                </Option>
              ))}
            </CustomScrollbar>
          </div>
        )}
      </div>
    );
  }
}

export default Select;
