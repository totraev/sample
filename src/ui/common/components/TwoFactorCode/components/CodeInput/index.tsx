import React, { PureComponent, FormEvent, KeyboardEvent } from 'react';
import './styles.sss';

import { IProps, IState, CodeValue } from './types';
import { BACKSPACE } from './constants';

class CodeInput extends PureComponent<IProps, IState> {
  private inputs: HTMLInputElement[] = [];
  public state: IState = {
    code: ['', '', '', '', '', ''],
    active: 0
  };
  public static defaultProps: IProps = {
    code: ['', '', '', '', '', ''],
    onChange: () => {},
    onFocus: () => {}
  };

  public static getDerivedStateFromProps(props: IProps, state: IState): IState {
    return props.code !== state.code
      ? { ...state, code: props.code }
      : null ;
  }

  public componentDidUpdate(): void {
    const { active } = this.state;

    if (active < this.inputs.length && this.state.code[active] === '') {
      this.inputs[active].focus();
    }
  }

  private handleChange = (index: number) => (e: FormEvent<HTMLInputElement>): void => {
    const { value } = e.currentTarget;

    if (/^\d?$/.test(value)) {
      const code = [
        ...this.state.code.slice(0, index),
        value,
        ...this.state.code.slice(index + 1)
      ] as CodeValue;

      this.setState({ code, active: index + 1 });
      this.props.onChange(code);
    }
  }

  private handleKeyDown = (index: number) => (e: KeyboardEvent<HTMLInputElement>): void => {
    if (e.keyCode === BACKSPACE) {
      const { value } = e.currentTarget;

      if (value === '' && index > 0) {
        const code = [
          ...this.state.code.slice(0, index - 1),
          value,
          ...this.state.code.slice(index)
        ] as CodeValue;

        this.setState({ code, active: index - 1 });
        this.props.onChange(code);
      }
    }
  }

  private handleFocus = (index: number) => (): void => {
    this.props.onFocus(index);
    this.setState(prevState => ({ ...prevState, active: index }));
  }

  private inputRef = (index: number) => (input: HTMLInputElement): void => {
    this.inputs[index] = input;
  }

  public render(): JSX.Element {
    const { code } = this.state;

    return (
      <div styleName="group">
        {[0, 1, 2, 3, 4, 5].map(index => (
          <div styleName="group-item" key={index}>
            <input
              styleName="input"
              value={code[index]}
              ref={this.inputRef(index)}
              onChange={this.handleChange(index)}
              onFocus={this.handleFocus(index)}
              onKeyDown={this.handleKeyDown(index)}
              type="tel"
              pattern="[0-9]"
              size={1}
              maxLength={1}
              autoComplete="off"
              tabIndex={0}
            />
          </div>
        ))}
      </div>
    );
  }
}

export default CodeInput;
