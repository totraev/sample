export type CodeValue = [string, string, string, string, string, string];

export interface IProps {
  code?: CodeValue;
  onChange?: (code: CodeValue) => void;
  onFocus?: (index: number) => void;
}

export interface IState {
  code: CodeValue;
  active: number;
}
