import { InjectedTranslateProps, InjectedI18nProps } from 'react-i18next';

export type CodeValue = [string, string, string, string, string, string];

export interface IProps extends InjectedTranslateProps, InjectedI18nProps {
  label: string;
  code: CodeValue;
  error?: string;
  twoFaEnabled?: boolean;
  onChange?: (code: CodeValue) => void;
  onFocus?: (index: number) => void;
  disabled?: boolean;
}
