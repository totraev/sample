import React, { SFC } from 'react';
import { translate, Trans } from 'react-i18next';
import { IProps } from './types';

import { Link } from 'react-router-dom';

import FieldSection from '@ui/common/components/FieldSection';
import InfoMsg from '@ui/common/components/InfoMsg';

import CodeInput from './components/CodeInput';

const TwoFactorCode: SFC<IProps> = ({
  label,
  code,
  error,
  twoFaEnabled,
  onChange,
  onFocus
}) => {
  if (!twoFaEnabled) {
    return (
      <InfoMsg>
        <Trans
          i18nKey="two_factor_code.info_msg"
          components={[<Link key="0" to="/user/settings" children="enable"/>]}
        />
      </InfoMsg>
    );
  }

  return (
    <FieldSection
      active
      label={label}
      error={error}
    >
      <CodeInput
        code={code}
        onChange={onChange}
        onFocus={onFocus}
      />
    </FieldSection>
  );
};

TwoFactorCode.defaultProps = {
  twoFaEnabled: true
};

export default (translate('common'))(TwoFactorCode);
