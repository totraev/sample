import React, { SFC } from 'react';

import TextField from '@ui/common/components/TextField';
import { IProps } from './types';

const NumberField: SFC<IProps> = (props) => {
  return (
    <TextField
      type="tel"
      {...props}
    />
  );
};

export default NumberField;
