
import { IOutterProps as IFieldOutterProps } from '@ui/common/components/TextField/types';

export interface IProps extends IFieldOutterProps {}
