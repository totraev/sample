import React, { SFC } from 'react';
import './styles.sss';

import classNames from 'classnames';

import { IProps } from './types';

const FieldSection: SFC<IProps> = (props) => {
  const {
    id,
    children,
    label,
    active,
    hint,
    error,
    disabled,
    className
  } = props;

  const wrapStyleName = classNames('field', {
    ['field--focus']: active,
    ['field--error']: Boolean(error),
    ['field--disabled']: disabled
  });

  return (
    <div styleName={wrapStyleName} className={className}>
      <label styleName="label" htmlFor={id}>{label}</label>
      {children}
      {Boolean(hint) && !Boolean(error) && <p styleName="hint">{hint}</p>}
      {Boolean(error) && <p styleName="error">{error}</p>}
    </div>
  );
};

FieldSection.defaultProps = {
  label: '',
  error: null,
  hint: null
};

export default FieldSection;
