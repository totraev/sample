import { HTMLProps } from 'react';

export interface IProps extends HTMLProps<HTMLDivElement>{
  label: string;
  active?: boolean;
  hint?: string | JSX.Element;
  error?: string;
}
