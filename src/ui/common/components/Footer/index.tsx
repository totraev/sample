import React, { SFC } from 'react';
import { translate } from 'react-i18next';
import './styles.sss';

import { IProps } from './types';
import config from '../../../../config';

const Footer: SFC<IProps> = ({ prepend, className, t }) => (
  <footer styleName="footer" className={className}>
    {prepend}

    <nav styleName="nav">
      {/*<Link styleName="nav-item" to="#">About</Link>*/}
      <a styleName="nav-item" href={config.specUrl}>{t('footer.docs')}</a>
      <a styleName="nav-item" href="/user/api">{t('footer.api')}</a>
    </nav>

    {/*<button styleName="dark"/>*/}
  </footer>
);

export default translate('common')(Footer);
