import { HTMLProps } from 'react';
import { InjectedI18nProps, InjectedTranslateProps } from 'react-i18next';

export interface IProps extends
  HTMLProps<HTMLDivElement>,
  InjectedI18nProps,
  InjectedTranslateProps {
  prepend?: JSX.Element;
}
