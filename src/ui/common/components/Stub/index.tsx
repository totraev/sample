import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Stub: SFC<IProps> = ({ stubTitle, message }) => (
  <div styleName="container">
    <div styleName="title">
      {stubTitle}
    </div>
    <div styleName="message">
      {message}
    </div>
  </div>
);

export default Stub;
