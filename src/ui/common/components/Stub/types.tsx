import { HTMLProps } from 'react';

export interface IProps extends HTMLProps<HTMLDivElement> {
  stubTitle: string;
  message: string;
}
