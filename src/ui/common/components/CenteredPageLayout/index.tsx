import React, { SFC } from 'react';
import './styles.sss';

const CenteredPageLayout: SFC = ({ children }) => (
  <div styleName="container">
      {children}
    </div>
);

export default CenteredPageLayout;
