import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const PageLayout: SFC<IProps> = ({ children, className }) => (
  <div styleName="container" className={className}>
    {children}
  </div>
);

export default PageLayout;
