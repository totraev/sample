import { IProps as ISelectProps } from '@ui/common/components/Select/types';

export interface IProps extends ISelectProps {
  label: string;
  error?: string;
}
