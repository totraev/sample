import React, { SFC } from 'react';

import FieldSection from '@ui/common/components/FieldSection';
import { IProps } from './types';

import Select from '@ui/common/components/Select';

const SelectField: SFC<IProps> = ({ label, error, ...restProps }) => (
  <FieldSection
    label={label}
    error={error}
    active
  >
    <Select {...restProps}/>
  </FieldSection>
);

export default SelectField;
