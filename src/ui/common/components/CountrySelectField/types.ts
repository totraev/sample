import { InjectedTranslateProps, InjectedI18nProps } from 'react-i18next';
import { IProps as ISelectFieldProps } from '@ui/common/components/SelectField/types';

export interface IProps extends ISelectFieldProps, InjectedTranslateProps, InjectedI18nProps {}
