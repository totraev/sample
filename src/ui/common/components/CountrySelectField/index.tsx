import React, { SFC } from 'react';
import { translate } from 'react-i18next';

import SelectField from '../SelectField';
import Country from '@ui/user/components/Country';

import { countries } from '@utils/countries';
import { IProps } from './types';

const options = countries.map(({ code }) => ({
  value: code.toLowerCase(),
  name: <Country code={code.toLowerCase()}/> as any
}));

const CountrySelectField: SFC<IProps> = props => (
  <SelectField
    {...props}
    options={options}
    placeholder={props.t('country_select.placeholder')}
  />
);

export default translate('common')(CountrySelectField);
