import React, { Component } from 'react';

import Scrollbars from 'react-custom-scrollbars';
import { IProps } from './types';

class CustomScrollbar extends Component<IProps> {
  private renderThumbVertical({ style }) {
    const styles = {
      ...style,
      width: '6px',
      borderRadius: '3px',
      backgroundColor: 'rgba(255, 255, 255, 0.1)'
    };

    return <div style={styles}/>;
  }

  private renderTrackVertical({ style }) {
    const styles = {
      ...style,
      right: '4px',
      top: '5px',
      bottom: '5px'
    };

    return <div style={styles}/>;
  }

  public render() {
    const { scrollbarRef, ...restProps } = this.props;

    return (
      <Scrollbars
        ref={scrollbarRef}
        autoHide={false}
        renderThumbVertical={this.renderThumbVertical}
        renderTrackVertical={this.renderTrackVertical}
        style={{ width: '100%', height: '100%' }}
        {...restProps}
      />
    );
  }
}

export default CustomScrollbar;
