import Scrollbars, { ScrollbarProps } from 'react-custom-scrollbars';
import { Ref } from 'react';

export interface IProps extends ScrollbarProps {
  scrollbarRef?: Ref<Scrollbars>;
}
