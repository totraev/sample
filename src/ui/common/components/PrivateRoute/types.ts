import { RouteProps } from 'react-router-dom';
import { RootStore } from '@stores/rootStore';

export interface IProps extends RouteProps {
  rootStore?: RootStore;
}
