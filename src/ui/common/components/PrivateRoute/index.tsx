import React, { SFC } from 'react';
import { inject, Observer } from 'mobx-react';
import { Route, Redirect } from 'react-router-dom';

import { IProps } from './types';

const PrivateRoute: SFC<IProps> = ({ rootStore, component: Component, ...routeProps }) => {
  const render = props => (
    <Observer>
      {() => rootStore.auth.isAuth
        ? <Component {...props}/>
        : <Redirect to="/auth/login"/>}
    </Observer>
  );

  return <Route {...routeProps} render={render}/>;
};

export default inject('rootStore')(PrivateRoute);
