import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const InfoMsg: SFC<IProps> = ({ children, className }) => (
  <div styleName="msg" className={className}>
    {children}
  </div>
);

export default InfoMsg;
