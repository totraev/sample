import { StateHandlerMap, StateHandler } from 'recompose';
import { IOutterProps as IFieldOutterProps } from '@ui/common/components/TextField/types';

export interface IOutterProps extends IFieldOutterProps {}

export interface IInnerState {
  visible: boolean;
}

export interface IStateHandlers {
  handleVisibility: StateHandler<IInnerState>;
}

export interface IStateUpdaters extends StateHandlerMap<IInnerState>, IStateHandlers {}

export interface IInnerProps extends IOutterProps, IStateHandlers, IInnerState {}
