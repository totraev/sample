import React, { SFC } from 'react';
import './styles.sss';

import { compose, withStateHandlers } from 'recompose';

import TextField from '@ui/common/components/TextField';
import { IInnerProps, IOutterProps, IInnerState, IStateUpdaters } from './types';

const PasswordField: SFC<IInnerProps> = ({ visible, handleVisibility, ...fieldProps }) => {
  const button = (
    <button
      styleName={visible ? 'button--visible' : 'button--hidden'}
      type="button"
      onClick={handleVisibility}
      tabIndex={-1}
    />
  );

  return (
    <TextField
      type={visible ? 'text' : 'password'}
      append={button}
      {...fieldProps}
    />
  );
};

const withComponentState = withStateHandlers<IInnerState, IStateUpdaters>(
  { visible: false },
  {
    handleVisibility: ({ visible }) => () => ({ visible: !visible })
  }
);

export default compose<IInnerProps, IOutterProps>(
  withComponentState
)(PasswordField);
