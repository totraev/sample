import React, { SFC } from 'react';
import './styles.sss';

const PageLoader: SFC = () => (
  <div styleName="loader">
    <div styleName="line-wrap">
      <span styleName="line-1"/>
      <span styleName="line-2"/>
      <span styleName="line-3"/>
      <span styleName="line-4"/>
      <span styleName="line-5"/>
    </div>
  </div>
);

export default PageLoader;
