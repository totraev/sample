import React, { SFC } from 'react';
import './styles.sss';

const Title: SFC = ({ children }) => (
    <h2 styleName="title">
      {children}
    </h2>
);

export default Title;
