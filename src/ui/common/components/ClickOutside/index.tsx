import React, { Component } from 'react';

import { IProps } from './types';

class ClickOutside extends Component<IProps> {
  private container: HTMLDivElement = null;
  private isTouch: boolean = false;

  public static defaultProps: IProps = {
    onClickOutside: () => {}
  };

  public componentDidMount(): void {
    document.addEventListener('touchend', this.handle, true);
    document.addEventListener('click', this.handle, true);
  }

  public componentWillUnmount(): void {
    document.removeEventListener('touchend', this.handle, true);
    document.removeEventListener('click', this.handle, true);
  }

  private getContainer = (ref: HTMLDivElement): void => {
    this.container = ref;
  }

  private handle = (e): void => {
    if (e.type === 'touchend') {
      this.isTouch = true;
    }

    if (e.type === 'click' && this.isTouch) {
      return;
    }

    const el = this.container;

    if (el && !el.contains(e.target)) {
      this.props.onClickOutside(e);
    }
  }

  render() {
    const { children, onClickOutside, ...props } = this.props;

    return <div {...props} ref={this.getContainer}>{children}</div>;
  }
}

export default ClickOutside;
