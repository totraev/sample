import { HTMLProps, MouseEventHandler } from 'react';

export interface IProps extends HTMLProps<HTMLDivElement> {
  onClickOutside?: MouseEventHandler<any>;
}
