import { InjectedTranslateProps, InjectedI18nProps } from 'react-i18next';

export interface IOutterProps {
  id: string;
}

export interface IHandlersProps extends IOutterProps {
  copied: boolean;
  setCopied: (value: boolean) => void;
}

export interface IHandlers {
  handleCopy: () => void;
}

export interface IProps extends
  IHandlers,
  IHandlersProps,
  IOutterProps,
  InjectedTranslateProps,
  InjectedI18nProps {}
