import React, { SFC } from 'react';
import { translate } from 'react-i18next';
import './styles.sss';

import CopyToClipboard from 'react-copy-to-clipboard';
import { compose, withState, withHandlers } from 'recompose';

import Tooltip from '@ui/common/components/Tooltip';

import { IProps, IOutterProps, IHandlersProps, IHandlers } from './types';

const ApiKeyId: SFC<IProps> = ({ id, copied, handleCopy, t }) => (
  <CopyToClipboard text={id} onCopy={handleCopy}>
    <span styleName="id">
      <Tooltip
        overlay={copied ?
          t('tooltip_copy_to_clipboard.copied') :
          t('tooltip_copy_to_clipboard.copy')}
        mouseLeaveDelay={0}
      >
        <span>{id}</span>
      </Tooltip>
    </span>
  </CopyToClipboard>
);

export default compose<IProps, IOutterProps>(
  translate('common'),
  withState<IOutterProps, boolean, 'copied', 'setCopied'>('copied', 'setCopied', false),
  withHandlers<IHandlersProps, IHandlers>(
  ({ setCopied }) => {
    let timeout: NodeJS.Timer = null;

    return {
      handleCopy: () => () => {
        clearTimeout(timeout);
        setCopied(true);
        timeout = setTimeout(() => setCopied(false), 2000);
      }
    };
  })
)(ApiKeyId);
