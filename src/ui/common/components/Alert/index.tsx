import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';

const Alert: SFC<IProps> = ({ type, children }) => (
    <div styleName={type}>
      {children}
    </div>
);

export default Alert;
