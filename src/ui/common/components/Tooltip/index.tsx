import React, { SFC } from 'react';
import RCTooltip from 'rc-tooltip';

import 'rc-tooltip/assets/bootstrap.css';

import { IProps } from './types';

const Tooltip: SFC<IProps> = props => <RCTooltip {...props}/>;

export default Tooltip;
