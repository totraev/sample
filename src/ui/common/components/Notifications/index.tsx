import React, { SFC } from 'react';
import { inject, observer } from 'mobx-react';
import './styles.sss';

import { compose } from 'recompose';

import { TransitionGroup } from 'react-transition-group';
import Notification from './components/Notification';
import FadeTransition from './components/FadeTransition';

import { IProps } from './types';

const Notifications: SFC<IProps> = ({ notificationsState }) => (
  <TransitionGroup styleName="notification-wrap">
    {notificationsState.notifications.map(model => (
      <FadeTransition key={model.time}>
        <Notification model={model}/>
      </FadeTransition>
    ))}
  </TransitionGroup>
);

export default compose<IProps, IProps>(
  inject('notificationsState'),
  observer
)(Notifications);
