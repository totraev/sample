import React, { SFC } from 'react';
const css = require('./styles.sss');

import { CSSTransition } from 'react-transition-group';

const classNames = {
  enter: css.show,
  enterActive: css.showActive,
  exit: css.close,
  exitActive: css.closeActive
};

const FadeTransition: SFC = props => (
  <CSSTransition
    {...props}
    classNames={classNames}
    timeout={{ enter: 500, exit: 500 }}
  />
);

export default FadeTransition;
