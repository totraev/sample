import { HTMLProps, MouseEventHandler } from 'react';
import NotificationModel from '@models/NotificationModel';

export interface IOutterProps extends HTMLProps<HTMLDivElement>  {
  model: NotificationModel;
}

export interface IHandlers {
  onClose: MouseEventHandler<HTMLButtonElement>;
}

export interface IProps extends IOutterProps, IHandlers {}
