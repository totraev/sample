import React, { SFC } from 'react';
import './styles.sss';

import { compose, withHandlers } from 'recompose';
import { IOutterProps, IProps, IHandlers } from './types';

const Notification: SFC<IProps> = ({ model, onClose, className }) => (
  <div styleName={model.type} className={className}>
    {model.message}

    <button
      styleName="close"
      onClick={onClose}
    />
  </div>
);

export default compose<IProps, IOutterProps>(
  withHandlers<IOutterProps, IHandlers>({
    onClose: props => () => {
      props.model.remove();
    }
  })
)(Notification);
