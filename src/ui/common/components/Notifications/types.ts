import { NotificationsState } from '@ui/common/states/notificationsState';

export interface IProps {
  notificationsState?: NotificationsState;
}
