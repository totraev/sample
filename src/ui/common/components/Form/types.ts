import { HTMLProps } from 'react';

export interface IProps extends HTMLProps<HTMLDivElement> {
  title: string;
  subtitle?: string | JSX.Element;
  error?: string;
  info?: string | JSX.Element;
  append?: string | JSX.Element;
}
