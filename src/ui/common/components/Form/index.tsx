import React, { SFC } from 'react';
import './styles.sss';

import { IProps } from './types';
import Alert from '@ui/common/components/Alert';

const Form: SFC<IProps> = ({ append, children, title, className, error, info }) => (
  <form styleName="container" className={className}>
    <div styleName="head">
      <h2 styleName="title">
        {title}
        {Boolean(append) && <span>{append}</span>}
      </h2>

      {Boolean(error) && <Alert type="error">{error}</Alert>}
      {Boolean(info) && !Boolean(error) && <Alert type="info">{info}</Alert>}
    </div>

    <div styleName="content">
      {children}
    </div>
  </form>
);

Form.defaultProps = {
  error: '',
  info: ''
};

export default Form;
