import { AppState } from '@ui/common/states/appState';

export interface IProps {
  appState?: AppState;
}
