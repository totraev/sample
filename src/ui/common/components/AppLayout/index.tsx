import React, { SFC } from 'react';
import './styles.sss';

import Header from '@ui/common/components/Header';

const AppLayout: SFC = ({ children }) => (
  <div styleName="app">
    <Header />
    {children}
  </div>
);

export default AppLayout;
