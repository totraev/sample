import React, { SFC } from 'react';
import './styles.sss';

import classNames from 'classnames';
import { compose, defaultProps, withStateHandlers } from 'recompose';
import FieldSection from '@ui/common/components/FieldSection';

import { IProps, IOutterProps, IInnerState, IStateUpdaters } from './types';

const TextField: SFC<IProps> = (props) => {
  const {
    label,
    active,
    handleBlur,
    handleFocus,
    hint,
    error,
    append,
    wrapClassName,
    className,
    ...inputProps
  } = props;

  const styleName = classNames('input', {
    ['input--error']: Boolean(error),
    ['input--append']: Boolean(append)
  });

  return (
    <FieldSection
      label={label}
      active={active || Boolean(inputProps.value)}
      hint={hint}
      error={error}
      disabled={inputProps.disabled}
      className={wrapClassName}
    >
      <div styleName="input-wrap">
        <input
          type="text"
          {...inputProps}
          styleName={styleName}
          className={className}
          onFocus={handleFocus}
          onBlur={handleBlur}
          disabled={inputProps.disabled}
        />

        {Boolean(append) && (
          <span styleName={Boolean(inputProps.disabled) ? 'append--disabled' : 'append'}>
            {append}
          </span>
        )}
      </div>
    </FieldSection>
  );
};

export default compose<IProps, IOutterProps>(
  defaultProps<IOutterProps>({
    label: '',
    error: null,
    hint: null,
    append: null,
    onFocus: () => {},
    onBlur: () => {}
  }),
  withStateHandlers<IInnerState, IStateUpdaters, IOutterProps>(
    { active: false },
    {
      handleFocus: (_, props) => (e) => {
        props.onFocus(e);

        return { active: true };
      },
      handleBlur: (_, props) => (e) => {
        props.onBlur(e);

        return { active: false };
      }
    }
  )
)(TextField);
