import React, { SFC } from 'react';
import './styles.sss';

const PageTitle: SFC = ({ children }) => (
  <h1 styleName="heading">
    {children}
  </h1>
);

export default PageTitle;
