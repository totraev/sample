import React, { SFC } from 'react';
import './styles.sss';

import classNames from 'classnames';
import TextareaAutosize from 'react-autosize-textarea';
import { compose, defaultProps, withStateHandlers } from 'recompose';

import FieldSection from '@ui/common/components/FieldSection';

import { IProps, IOutterProps, IInnerState, IStateUpdaters } from './types';

const TextAreaField: SFC<IProps> = (props) => {
  const {
    label,
    active,
    handleBlur,
    handleFocus,
    hint,
    error,
    wrapClassName,
    className,
    ...inputProps
  } = props;

  const isActive = active || Boolean(inputProps.value);

  const styleName = classNames('textarea-wrap', {
    ['textarea-wrap--active']: isActive,
    ['textarea-wrap--disabled']: Boolean(inputProps.disabled),
    ['textarea-wrap--error']: Boolean(error),
  });

  return (
    <FieldSection
      label={label}
      active={isActive}
      hint={hint}
      error={error}
      disabled={inputProps.disabled}
      className={wrapClassName}
    >
      <div styleName={styleName} >
        <TextareaAutosize
          styleName="textarea"
          className={className}
          onFocus={handleFocus}
          onBlur={handleBlur}
          {...inputProps}
        />
      </div>
    </FieldSection>
  );
};

export default compose<IProps, IOutterProps>(
  defaultProps<IOutterProps>({
    label: '',
    error: null,
    hint: null
  }),
  withStateHandlers<IInnerState, IStateUpdaters>(
    { active: false },
    {
      handleFocus: () => () => ({ active: true }),
      handleBlur: () => () => ({ active: false })
    }
  )
)(TextAreaField);
