import { AllHTMLAttributes } from 'react';
import { StateHandler, StateHandlerMap } from 'recompose';

export interface IOutterProps extends AllHTMLAttributes<HTMLTextAreaElement>{
  label: string;
  hint?: string | JSX.Element;
  error?: string;
  wrapClassName?: string;
}

export interface IInnerState {
  active: boolean;
}

export interface IStateHandlers {
  handleBlur: StateHandler<IInnerState>;
  handleFocus: StateHandler<IInnerState>;
}

export interface IStateUpdaters extends StateHandlerMap<IInnerState>, IStateHandlers {}

export interface IProps extends IOutterProps, IInnerState, IStateHandlers {
  active: boolean;
  setActive: (active: boolean) => void;
}
