export interface UserInfoParams {
  first_name: string;
  last_name: string;
  country: string;
}
