export interface Wallet {
  curr: string;
  address: string;
}

export interface Deposit {
  trans_id: number;
  curr: string;
  amount: string;
  done: boolean;
  time: number;
}

export interface Withdrawal {
  trans_id: number;
  curr: string;
  dest_address: string;
  amount: string;
  fee: string;
  done: boolean;
  time: number;
}

export interface WithdrawalParams {
  curr: string;
  amount: string;
  address: string;
  fee: string;
  pass_code?: string;
}
