export interface LoginParams {
  email: string;
  password: string;
  g_recaptcha_response: string;
  pass_code?: string;
}

export interface LoginResponse {
  id: string;
  secret: string;
  opaque_user_id: string;
}

export interface RegisterParams {
  email: string;
  password: string;
  password_repeat: string;
  g_recaptcha_response: string;
}

export interface RegisterResponse {
  api_key: LoginResponse;
}

export interface NewPasswordParams {
  email: string;
  g_recaptcha_response: string;
  pass_code?: string;
}

export interface TokenParams {
  token: string;
}

export interface TokenResponse {
  email: string;
}

export interface SetPasswordParams {
  token: string;
  password: string;
  password_repeat: string;
  pass_code?: string;
}

export interface ChangePasswordParams {
  old_password: string;
  new_password: string;
  new_password_repeat: string;
  pass_code: string;
}

export interface TwoFaEnabledResponse {
  '2fa_enabled': boolean;
}

export interface TwoFaEnableResponse {
  auth: string;
}
