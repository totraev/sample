export type OrderType = 'limit' | 'market' | 'stop';
export type TypeInForce = 'gtc' | 'ioc' | 'fok';

export interface Order {
  seq_num: number;
  action: L3Action;
  symbol: string;
  order_id: number;
  side: 'buy' | 'sell';
  type: OrderType;
  volume: string;
  original_volume: string;
  event_time: number;
  time_in_force?: TypeInForce;
  price?: string;
}

export interface CreateMarketOrder {
  symbol: string;
  volume: string;
  price?: string;
  side: 'buy' | 'sell';
  type: 'market';
  stop: boolean;
}

export interface CreateLimitOrder {
  symbol: string;
  volume: string;
  price: string;
  side: 'buy' | 'sell';
  type: 'limit';
  time_in_force: TypeInForce;
  post_only: boolean;
  stop: boolean;
}

export type CreateOrderPayload = CreateLimitOrder | CreateMarketOrder;

export interface OrderResponsePayload {
  order_id: number;
}

export interface CancelOrderPayload {
  symbol: string;
  order_id: number;
}

export interface OrderStatusPayload {
  symbol: string;
  order_id: number;
}

export interface OrderStatusResponse {
  order_id: number;
  symbol: string;
  side: 'sell' | 'buy';
  type: OrderType;
  price?: string;
  volume: string;
  original_volume: string;
  timestamp: number;
  is_live: boolean;
  is_canceled: boolean;
}

/* L2Updates */
export interface L2OrderPayload {
  seq_num: number;
  symbol: string;
  side: 'sell' | 'buy';
  price: string;
  volume: string;
  count: number;
  timestamp: number;
}

/* L3Updates */
export enum L3Action {
  Created = 1,
  Executed = 2,
  Canceled = 3
}

export interface L3OrderCreatedPayload {
  seq_num: number;
  action: L3Action.Created;
  symbol: string;
  order_id: number;
  side: 'buy' | 'sell';
  type: OrderType;
  price: string;
  volume: string;
  original_volume: string;
  event_time: number;
}

export interface L3OrderExecutedPayload {
  seq_num: number;
  action: L3Action.Executed;
  symbol: string;
  order_id: number;
  volume: string;
  original_volume: string;
  event_time: number;
}

export interface L3OrderCanceledPayload {
  seq_num: number;
  action: L3Action.Canceled;
  symbol: string;
  order_id: number;
  event_time: number;
}

export type L3OrderPayload =
  L3OrderCreatedPayload |
  L3OrderExecutedPayload |
  L3OrderCanceledPayload;

/* Candles */
export type CandleResolutions = '1m' | '5m' | '1h' | '1d' | '1w';

export interface CandlePayload {
  low: string;
  close: string;
  resolution: CandleResolutions;
  open: string;
  close_deal_id: number;
  time: number;
  volume: string;
  high: string;
  symbol: string;
}

export interface CandleParams {
  symbol: string;
  resolution: string;
  from: number;
  to: number;
}
