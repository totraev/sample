export interface IHTTPError {
  error_msg: string;
  error_code: number;
}

export class ServerError extends Error {
  public code: number;

  constructor(data: IHTTPError) {
    super(data.error_msg);

    this.code = data.error_code;
  }
}
