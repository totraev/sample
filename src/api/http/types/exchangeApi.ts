export interface CurrPair {
  name: string;
}

export interface FuturesSymbolDetail {
  symbol_type: 'futures';
  symbol: string;
  quote_curr: string;
  quote_curr_precision: number;
  price_curr: string;
  maker_fee: string;
  taker_fee: string;
  price_step: string;
  contract_type: 'linear' | 'inverse';
  contract_size: string;
  underlying_index: string;
  max_leverage: number;
  index_termination_payment: string;
}

export interface SpotSymbolDetail {
  symbol_type: 'spot';
  symbol: string;
  base_curr: string;
  quote_curr: string;
  quote_curr_precision: number;
  price_curr: string;
  maker_fee: string;
  taker_fee: string;
  price_step: string;
  volume_step: string;
}

export type SymbolDetail = SpotSymbolDetail | FuturesSymbolDetail;

export interface Balance {
  curr: string;
  balance: string;
  blocked: string;
}
