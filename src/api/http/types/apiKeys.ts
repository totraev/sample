export enum AccessLevel { View = 1, Trade, Withdraw }

export interface ApiKey {
  id: string;
  name: string;
  description: string;
  secret: string;
  level: AccessLevel;
}
