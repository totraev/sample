import { publicFetch } from './apiFetch';

class PublicApi {
  public get<T = any>(path: string, params?: any): Promise<T> {
    return publicFetch(path, 'GET', params);
  }

  public post<T = any>(path: string, body?: any): Promise<T> {
    return publicFetch(path, 'POST', body);
  }

  public delete<T = any>(path: string, body?: any): Promise<T> {
    return publicFetch(path, 'DELETE', body);
  }

  public put<T = any>(path: string, body?: any): Promise<T> {
    return publicFetch(path, 'PUT', body);
  }

  public patch<T = any>(path: string, body?: any): Promise<T> {
    return publicFetch(path, 'PATCH', body);
  }
}

export default new PublicApi();
