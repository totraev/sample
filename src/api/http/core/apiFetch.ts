import Hex from 'crypto-js/enc-hex';
import hmacSHA512 from 'crypto-js/hmac-sha512';

import rootStore from '@stores/rootStore';
import { milliSecToNanoSec } from '@utils/time';

import { IHTTPError, ServerError } from '../types/core';
import { httpFullUrl, httpFullPath } from '@utils/api';

function checkHttpStatus(response: Response): Promise<any> | Response {
  if (response.ok) {
    return response;
  }

  return response.json().then((data: IHTTPError) => Promise.reject(new ServerError(data)));
}

function parseJSON(response: Response): Promise<any> | void {
  return response.text().then(text => Boolean(text) ? JSON.parse(text) : {});
}

export function publicFetch<T = any>(path: string, method: string, params?: any): Promise<T> {
  const apiPath = httpFullUrl(path);

  return fetch(apiPath, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(params)
  })
    .then(checkHttpStatus)
    .then(parseJSON);
}

export function privateFetch<T = any>(path: string, method: string, params: any = {}): Promise<T> {
  if (!rootStore.auth.isAuth) {
    return Promise.reject(new Error('you\'re not authorized'));
  }

  const apiPath = httpFullPath(path);
  const apiUrl = httpFullUrl(path);
  const payload = {
    timestamp: milliSecToNanoSec(Date.now()),
    ...params
  };
  const body = JSON.stringify(payload);
  const signature = Hex.stringify(hmacSHA512(apiPath + body, rootStore.auth.authData.secret));

  return fetch(apiUrl, {
    method,
    body,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      APIKEY: rootStore.auth.authData.apiKey,
      SIGNATURE: signature
    }
  })
    .then(checkHttpStatus)
    .then(parseJSON);
}
