import { privateFetch } from './apiFetch';

class PrivateApi {
  public get<T = any>(path: string, params?: any): Promise<T> {
    return privateFetch(path, 'GET', params);
  }

  public post<T = any>(path: string, body?: any): Promise<T> {
    return privateFetch(path, 'POST', body);
  }

  public delete<T = any>(path: string, body?: any): Promise<T> {
    return privateFetch(path, 'DELETE', body);
  }

  public put<T = any>(path: string, body?: any): Promise<T> {
    return privateFetch(path, 'PUT', body);
  }

  public patch<T = any>(path: string, body?: any): Promise<T> {
    return privateFetch(path, 'PATCH', body);
  }
}

export default new PrivateApi();
