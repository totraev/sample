import privateApi from './core/privateApi';

import { Wallet, Deposit, WithdrawalParams } from './types/walletsApi';

export function create(curr: string): Promise<Wallet> {
  return privateApi.post('/wallets/generate', { curr });
}

export function wallets(): Promise<Wallet[]> {
  return privateApi.post('/wallets/list');
}

export function depositHistory(): Promise<Deposit[]> {
  return privateApi.post('/wallets/deposit_history');
}

export function withdrawalHistory(): Promise<Deposit[]> {
  return privateApi.post('/wallets/withdrawal_history');
}

export function withdraw(params: WithdrawalParams): Promise<void> {
  return privateApi.post('/wallets/withdraw', params);
}
