import publicApi from './core/publicApi';
import privateApi from './core/privateApi';

import {
  CreateOrderPayload,
  OrderResponsePayload,
  Order,
  CancelOrderPayload,
  OrderStatusPayload,
  L2OrderPayload,
  L3OrderPayload,
  CandlePayload,
  CandleParams,
  OrderStatusResponse
} from './types/ordersApi';

export function createOrder(order: CreateOrderPayload): Promise<OrderResponsePayload> {
  return privateApi
    .post<OrderResponsePayload>('/order/new', order);
}

export function cancelOrder(order: CancelOrderPayload): Promise<void> {
  return privateApi
    .post<void>('/order/cancel', order);
}

export function cancelAllOrders(currPair: string): Promise<void> {
  return privateApi
    .post<void>('/order/cancel/all', { symbol: currPair });
}

export function cancelAllStopOrders(currPair: string): Promise<void> {
  return privateApi
    .post<void>('/order/cancel_all_stop', { symbol: currPair });
}

export function orderStatus(order: OrderStatusPayload): Promise<OrderStatusResponse> {
  return privateApi
    .post<OrderStatusResponse>('/order/status', order);
}

export function allOrders(): Promise<Order[]> {
  return privateApi
    .get<Order[]>('/orders');
}

export function l2Snapshot(): Promise<L2OrderPayload[]> {
  return publicApi
    .post<L2OrderPayload[]>('/book/l2');
}

export function l3Snapshot(): Promise<L3OrderPayload[]> {
  return publicApi
    .post<L3OrderPayload[]>('/book/l3');
}

export function candles(params: CandleParams): Promise<CandlePayload[]> {
  return publicApi
    .post<CandlePayload[]>('/candles', params);
}

export function indexTerminate(symbol: string): Promise<void> {
  return privateApi.post<void>('/order/index_terminate', { symbol });
}

export * from './types/ordersApi';
