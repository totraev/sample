import publicApi from './core/publicApi';
import privateApi from './core/privateApi';

import {
  RegisterParams,
  RegisterResponse,
  LoginParams,
  LoginResponse,
  NewPasswordParams,
  TokenParams,
  TokenResponse,
  SetPasswordParams,
  ChangePasswordParams,
  TwoFaEnabledResponse,
  TwoFaEnableResponse
} from './types/authApi';

export function register(params: RegisterParams): Promise<RegisterResponse> {
  return publicApi.post('/users/register', params);
}

export function login(params: LoginParams): Promise<LoginResponse> {
  return publicApi.post('/users/login', params);
}

export function logout(): Promise<void> {
  return privateApi.post('/users/logout');
}

export function newPassword(params: NewPasswordParams): Promise<void> {
  return publicApi.post('/users/new_password', params);
}

export function checkToken(params: TokenParams): Promise<TokenResponse> {
  return publicApi.post('/users/check_token', params);
}

export function setPassword(params: SetPasswordParams): Promise<void> {
  return publicApi.post('/users/set_password', params);
}

export function changePassword(params: ChangePasswordParams): Promise<void> {
  return privateApi.post('/users/change_password', params);
}

export function twoFaStatus(): Promise<TwoFaEnabledResponse> {
  return privateApi.post('/2fa/status');
}

export function twoFaGenerate(): Promise<TwoFaEnableResponse> {
  return privateApi.post('/2fa/generate');
}

export function twoFaEnable(code: string): Promise<TwoFaEnableResponse> {
  return privateApi.post('/2fa/enable', { pass_code: code });
}

export function twoFaDisable(code: string): Promise<void> {
  return privateApi.post('/2fa/disable', { pass_code: code });
}
