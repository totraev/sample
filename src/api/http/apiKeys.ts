import privateApi from './core/privateApi';

import { ApiKey, AccessLevel } from './types/apiKeys';

export function create(
  name: string,
  level: AccessLevel,
  description?: string,
  code?: string
): Promise<ApiKey> {
  return privateApi.post('/keys/new', { name, description, level, pass_code: code });
}

export function revoke(id: string): Promise<void> {
  return privateApi.post('/keys/revoke', { id });
}

export function list(): Promise<ApiKey[]> {
  return privateApi.post('/keys/list');
}
