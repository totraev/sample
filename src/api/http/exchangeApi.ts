import publicApi from './core/publicApi';
import privateApi from './core/privateApi';

import { CurrPair, SymbolDetail, Balance } from './types/exchangeApi';

export function status(): Promise<void> {
  return publicApi.get<void>('/status');
}

export function getSymbols(): Promise<CurrPair[]> {
  return publicApi.get<CurrPair[]>('/symbols');
}

export function symbolDetails(): Promise<SymbolDetail[]> {
  return publicApi.get<SymbolDetail[]>('/symbol_details');
}

export function balances(): Promise<Balance[]> {
  return privateApi.post<Balance[]>('/balances');
}

export * from './types/exchangeApi';
