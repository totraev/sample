import privateApi from './core/privateApi';

import { UserInfoParams } from './types/userApi';

export function getUserInfo(): Promise<UserInfoParams> {
  return privateApi.post('/users/info');
}

export function setUserInfo(params: UserInfoParams): Promise<void> {
  return privateApi.post('/users/set_info', params);
}
