import NanoEvents from 'nanoevents';

import { RequestMessage, Message } from './types/wsClient';
import { wsFullPath, wsFullUrl } from '@utils/api';

export interface IEmmiter {
  open: void;
  closed: boolean;
  connecting: void;
  message: Message;
  error: any;
}

export class WebSocketClient {
  public status: 'open' | 'closed' | 'connecting' = 'closed';
  public readonly URI: string;
  public readonly path: string;
  private socket: WebSocket = null;
  private emmiter = new NanoEvents<IEmmiter>();

  constructor() {
    this.URI = wsFullUrl('/ws');
    this.path = wsFullPath('/ws');

    this.emmiter.on('error', e => console.log(e));
  }

  public connect(): void {
    if (this.socket !== null) {
      this.disconnect();
    }

    this.socket = new WebSocket(this.URI);

    this.socket.onmessage = this.handleMessage;
    this.socket.onopen = this.handleOpen;
    this.socket.onclose = this.handleClose;
    this.socket.onerror = this.handleError;

    this.status = 'connecting';
    this.emmiter.emit('connecting', null);
  }

  public disconnect(wasClean: boolean = true): void {
    if (this.socket !== null) {
      this.socket.onclose(new CloseEvent('close', { wasClean }));
      this.socket.close(1000);

      this.socket.onmessage = null;
      this.socket.onopen = null;
      this.socket.onclose = null;
      this.socket.onerror = null;

      this.socket = null;
    }
  }

  public send(msg: RequestMessage): void {
    this.socket.send(JSON.stringify(msg));
  }

  public on(e: keyof IEmmiter, handler: (arg: IEmmiter[keyof IEmmiter]) => void): () => void {
    return this.emmiter.on(e, handler);
  }

  private handleOpen = (): void => {
    this.status = 'open';
    this.emmiter.emit('open', null);
  }

  private handleClose = (data: CloseEvent): void => {
    if (!data.wasClean) {
      this.emmiter.emit('error', data);
    }

    this.emmiter.emit('closed', data.wasClean);
    this.status = 'closed';
  }

  private handleMessage = ({ data }: MessageEvent): void => {
    this.emmiter.emit('message', JSON.parse(data));
  }

  private handleError = (data): void => {
    this.emmiter.emit('error', data);
  }
}

export default new WebSocketClient();
