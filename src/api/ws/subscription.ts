import { observable, action } from 'mobx';
import { chunkProcessor, IDisposer } from 'mobx-utils';
import shallowEqual from 'shallowequal';

import { Subscription, SubscriptionParams } from './types/wsClient';

class SubscriptionClient {
  private subscriptions: Subscription[] = [];

  private subscriptionsQueue = observable.array<Subscription>([], { deep: false });
  private cancelingSubscriptionsQueue = observable.array<Subscription>([], { deep: false });

  private subscriptionsDisposer: IDisposer = null;
  private cancelingSubscriptionsDisposer: IDisposer = null;

  public get activeSubscriptions(): Subscription[] {
    return Array.from(this.subscriptions);
  }

  public get hasActiveSubscriptions(): boolean {
    return this.subscriptions.length > 0;
  }

  public initSubscriptionsProcessor(
    processor: (subscriptions: Subscription[]) => void
  ): void {
    this.subscriptionsDisposer = chunkProcessor<Subscription>(
      this.subscriptionsQueue,
      processor,
      100
    );
  }

  public initCancelingSubscriptionsProccesor(
    processor: (subscriptions: Subscription[]) => void
  ): void {
    this.cancelingSubscriptionsDisposer = chunkProcessor<Subscription>(
      this.cancelingSubscriptionsQueue,
      processor,
      100
    );
  }

  public deinitSubscriptionsProcessor(): void {
    if (this.subscriptionsDisposer !== null) {
      this.subscriptionsDisposer();
    }
  }

  public deinitCancelingSubscriptionsProccesor(): void {
    if (this.cancelingSubscriptionsDisposer !== null) {
      this.cancelingSubscriptionsDisposer();
    }
  }

  @action public subscribe(channel: string, params: SubscriptionParams): void {
    this.subscriptionsQueue.push({ channel, params });
  }

  @action public unsubscribe(channel: string, params: SubscriptionParams): void {
    this.cancelingSubscriptionsQueue.push({ channel, params });
  }

  public onSubscribe(channel, params): void {
    this.subscriptions.push(channel, params);
  }

  public onUnsubscribe(channel, params): void {
    const index = this.subscriptions.findIndex((s) => {
      return s.channel === channel && shallowEqual(s.params, params);
    });

    if (index > -1) {
      this.subscriptions.splice(index, 1);
    }
  }

  public clear(): void {
    this.subscriptions = [];
  }
}

export default SubscriptionClient;
