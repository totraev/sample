import { WSApi } from './api';
import lifecycle from 'page-lifecycle';

export interface ConnectionOptions {
  delay: number;
  attempts: number;
}

export class ReconnectionClient {
  private options: ConnectionOptions = {
    delay: 5000,
    attempts: Infinity
  };

  private heartbeatTimer: NodeJS.Timer = null;
  private reconnectTimer: NodeJS.Timer = null;

  constructor(private wsApi: WSApi, options: Partial<ConnectionOptions> = {}) {
    this.options = Object.assign(this.options, options);

    this.wsApi.on('online', this.handleOnline);
    this.wsApi.on('offline', this.handleOffline);
    this.wsApi.on('disconnected', this.handleDisconnect);
    this.wsApi.onData('heartbeat', this.handleHeartbeat);

    lifecycle.addEventListener('statechange', this.handlePageStateChange);
  }

  private handleOnline = (): void => {
    clearTimeout(this.reconnectTimer);

    this.reconnectTimer = setTimeout(() => this.wsApi.connect(), this.options.delay);
  }

  private handleOffline = (): void => {
    console.log('offline');
    this.wsApi.disconnect(true);
  }

  private handleDisconnect = (wasClean: boolean): void => {
    clearTimeout(this.heartbeatTimer);
    clearTimeout(this.reconnectTimer);

    if (this.options.attempts > 0 && !wasClean) {
      this.reconnectTimer = setTimeout(
        () => {
          console.log('connect reconnect');
          this.wsApi.connect();
        },
        this.options.delay
      );
    }
  }

  private handlePageStateChange = (e): void => {
    if (e.newState === 'frozen') {
      return this.wsApi.disconnect(true);
    }

    if (e.newState === 'passive' && e.oldState !== 'active') {
      return this.wsApi.connect();
    }
  }

  private handleHeartbeat = (): void => {
    clearTimeout(this.heartbeatTimer);

    this.heartbeatTimer = setTimeout(
      () => this.wsApi.disconnect(false),
      10 * 1000
    );
  }
}

export default ReconnectionClient;
