import NanoEvents from 'nanoevents';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Hex from 'crypto-js/enc-hex';

import {
  Subscription,
  SubscribeMessage,
  UnsubscribeMessage,
  AuthMessage,
  DeauthMessage,
  SubscribeResponseMessage,
  UnsubscribeResponseMessage,
  HeartbeatMessage
} from './types/wsClient';

import { BalanceMessage } from './types/balances';
import { CandleMessage } from './types/candlestick';
import { L2Message } from './types/l2updates';
import { L3Message } from './types/l3Private';
import { PositionMessage } from './types/positions';
import { StopOrderMessage } from './types/stopOrders';
import { TickerMessage } from './types/ticker';
import { TradeMessage } from './types/trades';
import { TradePrivateMessage } from './types/tradesPrivate';

import { milliSecToNanoSec } from '@utils/time';
import ws, { WebSocketClient } from './ws';

export interface IStateEmmiter {
  online: void;
  offline: void;
  connected: void;
  disconnected: boolean;
  connecting: void;
  auth: void;
  deauth: void;
  subscribe: SubscribeResponseMessage;
  unsubscribe: UnsubscribeResponseMessage;
}

export interface IDataEmmiter {
  l2: L2Message;
  l3: L3Message;
  trades: TradeMessage;
  ticker: TickerMessage;
  candles: CandleMessage;
  l3_private: L3Message;
  balances: BalanceMessage;
  positions: PositionMessage;
  trades_private: TradePrivateMessage;
  stop_orders: StopOrderMessage;
  heartbeat: HeartbeatMessage;
}

export class WSApi {
  private stateEmmiter = new NanoEvents<IStateEmmiter>();
  private dataEmmiter = new NanoEvents<IDataEmmiter>();

  constructor (private ws: WebSocketClient) {
    window.addEventListener('online', () => this.stateEmmiter.emit('online', null));
    window.addEventListener('offline', () => this.stateEmmiter.emit('offline', null));

    this.ws.on('open', () => this.stateEmmiter.emit('connected', null));
    this.ws.on('closed', wasClean => this.stateEmmiter.emit('disconnected', wasClean));
    this.ws.on('connecting', () => this.stateEmmiter.emit('connecting', null));
    this.ws.on('message', this.handleMessage);
  }

  public connect(): void {
    if (this.ws.status === 'closed') {
      this.ws.connect();
    }
  }

  public disconnect(wasClean: boolean): void {
    if (this.ws.status !== 'closed') {
      this.ws.disconnect(wasClean);
    }
  }

  public auth(apiKey: string, secret: string): void {
    const timestamp = milliSecToNanoSec(Date.now()).toString();
    const signature = Hex.stringify(hmacSHA512(`${this.ws.path}${timestamp}`, secret));

    const authMessage: AuthMessage = {
      type: 'auth',
      payload: {
        timestamp,
        signature,
        api_key: apiKey
      }
    };

    this.ws.send(authMessage);
  }

  public deauth() {
    const deauthMessage: DeauthMessage = {
      type: 'deauth'
    };

    this.ws.send(deauthMessage);
  }

  public subscribe(subscriptions: Subscription[]): void {
    const subscribeMessage: SubscribeMessage = {
      type: 'subscribe',
      payload: { subscriptions }
    };

    this.ws.send(subscribeMessage);
  }

  public unsubscribe(subscriptions: Subscription[]): void {
    const unsubscribeMessage: UnsubscribeMessage = {
      type: 'unsubscribe',
      payload: { subscriptions }
    };

    this.ws.send(unsubscribeMessage);
  }

  public on(
    e: keyof IStateEmmiter,
    handler: (arg: IStateEmmiter[keyof IStateEmmiter]) => void
  ): () => void {
    return this.stateEmmiter.on(e, handler);
  }

  public onData(
    e: keyof IDataEmmiter,
    handler: (arg: IDataEmmiter[keyof IDataEmmiter]) => void
  ): () => void {
    return this.dataEmmiter.on(e, handler);
  }

  private handleMessage = (message: any): void => {
    switch (message.type) {
      case 'update':
      case 'snapshot':
        return this.dataEmmiter.emit(message.channel, message);
      case 'heartbeat':
        return this.dataEmmiter.emit('heartbeat', message);
      case 'auth_response':
        if (message.payload.success) {
          this.stateEmmiter.emit('auth', null);
        }
        return;
      case 'deauth_response':
        if (message.payload.success) {
          this.stateEmmiter.emit('deauth', null);
        }
        return;
      case 'subscribe_response':
        return this.stateEmmiter.emit('subscribe', message);
      case 'unsubscribe_response':
        return this.stateEmmiter.emit('unsubscribe', message);
    }
  }
}

export default new WSApi(ws);
