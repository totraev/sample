import { Message } from './wsClient';

export type Resolution = '1m' | '5m' | '1h' | '1d' | '1w';

export interface SubscribeParams {
  resolution: Resolution;
  symbol: string;
}

export interface CandlePayload {
  resolution: Resolution;
  close_deal_id: number;
  time: number;
  open: string;
  close: string;
  low: string;
  high: string;
  volume: string;
  symbol: string;
}

export interface CandleSnapshotMessage extends Message<'snapshot', CandlePayload[]> {}
export interface CandleUpdateMessage extends Message<'update', CandlePayload> {}
export type CandleMessage = CandleSnapshotMessage | CandleUpdateMessage;
