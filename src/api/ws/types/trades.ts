import { Message } from './wsClient';

export interface Trade {
  taker_id: number;
  maker_id : number;
  side: 'buy' | 'sell';
  volume: string;
  price: string;
  symbol: string;
  id: number;
  event_time: number;
}

export interface TradeSnapshotMessage extends Message<'snapshot', Trade[]> {}
export interface TradeUpdateMessage extends Message<'update', Trade> {}
export type TradeMessage = TradeSnapshotMessage | TradeUpdateMessage;
