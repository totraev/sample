import { Message } from  './wsClient';

export interface StopOrderAccepted {
  price: string;
  volume: string;
  order_id: number;
  event_time: number;
  action: 'accepted';
  symbol: string;
  side: 'sell' | 'buy';
}

export interface StopOrderRefused {
  symbol: string;
  order_id: number;
  event_time: number;
  action: 'refused';
  text: string;
}

export interface StopOrderDeleted {
  event_time: number;
  symbol: string;
  order_id: number;
  action: 'deleted';
}

export interface StopOrderTriggered {
  event_time: number;
  symbol: string;
  action: 'triggered';
  order_id: number;
}

export type StopOrderPayload = StopOrderAccepted
  | StopOrderRefused
  | StopOrderDeleted
  | StopOrderTriggered;

export interface StopOrderSnapshotMessage extends Message<'snapshot', StopOrderAccepted[]> {}
export interface StopOrderUpdateMessage extends Message<'update', StopOrderPayload> {}
export type StopOrderMessage = StopOrderSnapshotMessage | StopOrderUpdateMessage;
