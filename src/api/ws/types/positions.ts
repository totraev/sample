import { Message } from './wsClient';

export interface Position {
  symbol: string;
  position: string;
  rpnl: string;
  upnl: string;
  open_price: string;
  margin: string;
  liquidation_price: string;
  effective_leverage: string;
  event_time: number;
}

export interface PositionSnapshotMessage extends Message<'snapshot', Position[]> {}
export interface PositionUpdateMessage extends Message<'update', Position> {}
export type PositionMessage = PositionSnapshotMessage | PositionUpdateMessage;
