import { Message } from './wsClient';

export interface Trade {
  symbol: string;
  trade_id: number;
  order_id: number;
  order_price: string;
  order_type: 'market' | 'limit' | 'stop';
  initial_volume: string;
  remaining_volume: string;
  user_order_side: 'buy' | 'sell';
  trade_volume: string;
  trade_price: string;
  trade_time: number;
  rpnl: string;
  rpnl_calculated: boolean;
  fee: string;
  is_maker: boolean;
}

export interface TradeSnapshot extends Message<'snapshot', Trade[]> {}
export interface TradeUpdate extends Message<'update', Trade> {}
export type TradePrivateMessage = TradeSnapshot | TradeUpdate;
