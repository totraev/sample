
export interface RequestMessage<T extends string = string, P = any> {
  type: T;
  payload?: P;
}

/**
 * Subscribe / Unsubscribe
 */
export interface SubscriptionParams {
  symbol: string;
  [key: string]: string;
}

export interface Subscription {
  channel: string;
  params?: SubscriptionParams;
}

export interface SubscribePayload {
  subscriptions: Subscription[];
}

export interface SubscribeMessage extends RequestMessage<'subscribe', SubscribePayload> {}
export interface UnsubscribeMessage extends RequestMessage<'unsubscribe', SubscribePayload> {}

export interface SubscriptionResponse {
  success: boolean;
  channel: string;
  params?: any;
  error?: string;
}

export interface SubscribeResponsePayload {
  subscriptions: SubscriptionResponse[];
}

export interface SubscribeResponseMessage
  extends RequestMessage<'subscribe_response', SubscribeResponsePayload> {}

export interface UnsubscribeResponseMessage
  extends RequestMessage<'unsubscribe_response', SubscribeResponsePayload> {}

/**
 * Auth
 */
export interface AuthPayload {
  api_key: string;
  signature: string;
  timestamp: string;
}

export interface AuthMessage extends RequestMessage<'auth', AuthPayload> {}

export interface AuthResponsePayload {
  success: boolean;
  api_key: string;
  error?: string;
}

export interface AuthResponseMessage extends RequestMessage<'auth_response', AuthResponsePayload> {}

/**
 * Deauth
 */
export interface DeauthMessage extends RequestMessage<'deauth', void> {}

export interface DeauthResponsePayload {
  success: boolean;
  error?: string;
}

export interface DeauthResponseMessage
  extends RequestMessage<'deauth_response', DeauthResponsePayload> {}

/** Hearbeat */
export interface HeartbeatMessage {
  type: 'heartbeat';
  seq_num: number;
}

/** Message */
export interface Message<T extends string = string, P = any> {
  type: T;
  channel: string;
  payload: P;
  params: SubscriptionParams;
}
