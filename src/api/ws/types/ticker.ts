import { Message } from './wsClient';

export interface TickerPayload {
  symbol: string;
  last_trade_id: number;
  time: number;
  last_trade_price: string;
  price_24h_ago: string;
  low_price_24h: string;
  high_price_24h: string;
  volume_24h: string;
  mark_price: string;
}

export interface TickerSnapsotMessage extends Message<'snapshot', TickerPayload> {}
export interface TickerUpdateMessage extends Message<'update', TickerPayload> {}
export type TickerMessage = TickerSnapsotMessage | TickerUpdateMessage;
