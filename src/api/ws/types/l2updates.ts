import { Message } from './wsClient';

export interface PriceLevel {
  seq_num: number;
  symbol: string;
  side: 'sell' | 'buy';
  price: string;
  volume: string;
  count: number;
  timestamp: number;
}

export interface L2SnapshotPayload {
  snapshot: PriceLevel[];
  updates: PriceLevel[];
}

export interface L2SnapshotMessage extends Message<'snapshot', L2SnapshotPayload> {}
export interface L2UpdateMessage extends Message<'update', PriceLevel>{}
export type L2Message = L2SnapshotMessage | L2UpdateMessage;
