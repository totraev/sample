import { Message } from './wsClient';

export interface Balance {
  curr: string;
  balance: string;
  blocked: string;
  upnl: string;
}

export interface BalanceSnapshot extends Message<'snapshot', Balance[]> {}
export interface BalanceUpdate extends Message<'update', Balance> {}
export type BalanceMessage = BalanceSnapshot | BalanceUpdate;
