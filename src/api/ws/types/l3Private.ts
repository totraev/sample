import { Message } from './wsClient';

export type OrderType = 'limit' | 'market';

export type OrderTIM = 'gtc' | 'ioc' | 'fok';

export enum L3Action { Created = 1, Executed, Canceled }

export interface L3OrderCreatedPayload {
  seq_num: number;
  action: L3Action.Created;
  symbol: string;
  order_id: number;
  side: 'buy' | 'sell';
  type: OrderType;
  time_in_force: OrderTIM;
  price?: string;
  volume: string;
  initial_volume: string;
  event_time: number;
  cl_ord_id: string;
}

export interface L3OrderExecutedPayload {
  seq_num: number;
  action: L3Action.Executed;
  symbol: string;
  order_id: number;
  volume: string;
  original_volume: string;
  event_time: number;
}

export interface L3OrderCanceledPayload {
  seq_num: number;
  action: L3Action.Canceled;
  symbol: string;
  order_id: number;
  event_time: number;
}

export type L3UpdatePayload = L3OrderCanceledPayload
  | L3OrderCreatedPayload
  | L3OrderExecutedPayload;

export interface L3SnapshotPayload {
  snapshot: L3OrderCreatedPayload[];
  updates: L3UpdatePayload[];
}

export interface L3SnapshotMessage extends Message<'snapshot', L3SnapshotPayload> {}
export interface L3UpdateMessage extends Message<'update', L3UpdatePayload> {}
export type L3Message = L3SnapshotMessage | L3UpdateMessage;
