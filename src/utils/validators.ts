export function isNumber(value: string, decimalPlaces: number): boolean {
  const regExp = decimalPlaces > 0
    ? new RegExp(`^(?:\\d+\\.?\\d{0,${decimalPlaces}})?$`)
    : /^(?:\d+)?$/;

  return regExp.test(value);
}

export function isBitcoinAddress(value: string): boolean {
  return /^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$/.test(value);
}
