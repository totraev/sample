import config from '../config';

const { ssl, apiHost, apiVersion } = config;

export function httpFullUrl(path: string): string {
  const protocol = ssl ? 'https' : 'http';
  const formatedPath = path[0] === '/' ? path.substring(1) : path;

  return `${protocol}://${apiHost}/${apiVersion}/${formatedPath}`;
}

export function httpFullPath(path: string): string {
  const formatedPath = path[0] === '/' ? path.substring(1) : path;

  return `/${apiVersion}/${formatedPath}`;
}

export function wsFullUrl(path: string): string {
  const protocol = ssl ? 'wss' : 'ws';
  const formatedPath = path[0] === '/' ? path.substring(1) : path;

  return `${protocol}://${apiHost}/${apiVersion}/${formatedPath}`;
}

export function wsFullPath(path: string): string {
  const formatedPath = path[0] === '/' ? path.substring(1) : path;

  return `/${apiVersion}/${formatedPath}`;
}
