const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const YAML = require('yamljs');
const FaviconPlugin = require('favicons-webpack-plugin');

const ROOT_DIR = path.resolve(__dirname, '../../');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const SRC_DIR = path.resolve(ROOT_DIR, 'src');
const VENDOR_DIR = path.resolve(SRC_DIR, 'vendor');

module.exports = {
  entry: [SRC_DIR],

  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    modules: ['node_modules', SRC_DIR],
    plugins: [new TsconfigPathsPlugin({})]
  },

  plugins: [
    new FaviconPlugin(path.resolve(SRC_DIR, 'assets/images/favicon.png')),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        SSL: JSON.stringify(process.env.SSL),
        API_HOST: JSON.stringify(process.env.API_HOST),
        API_VERSION: JSON.stringify(process.env.API_VERSION),
        DEFAULT_SYMBOL: JSON.stringify(process.env.DEFAULT_SYMBOL),
        RECAPTCHA_SITE_KEY: JSON.stringify(process.env.RECAPTCHA_SITE_KEY),
        DOCS_URL: JSON.stringify(process.env.DOCS_URL),
        SPEC_URL: JSON.stringify(process.env.SPEC_URL),
        GTM_ID: JSON.stringify(process.env.GTM_ID),
        YM_ID: JSON.stringify(process.env.YM_ID)
      }
    }),
    new CopyWebpackPlugin([{
      from: VENDOR_DIR + '/tradingView/static',
      to: DIST_DIR + '/tradingView/static'
    }, {
      from: SRC_DIR + '/locales/**/*.yml',
      to: DIST_DIR + '/[path]/[name].json',
      transform: content => JSON.stringify(YAML.parse(content.toString())),
      toType: 'template',
      context: SRC_DIR
    }])
  ],

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
            }
          }
        ]
      },
      {
        test:   /\.(ttf|otf|eot|svg|woff2?)(\?.+)?$/,
        loader: 'url-loader',
        options:  {
          limit: 10000
        }
      },
      {
        test: /\.(jpe?g|png|gif|ico)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      }
    ]
  }
};
