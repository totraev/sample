const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const common = require('./webpack.common.js');

const ROOT_DIR = path.resolve(__dirname, '../../');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');

const devConfig = {
  mode: 'development',
  entry: ['webpack-hot-middleware/client?reload=true'],
  devtool: 'inline-source-map',
  target: 'web',

  output: {
    path: DIST_DIR,
    publicPath: '/',
    filename: 'bundle.[hash].js'
  },

  module: {
    rules: [
      {
        test: /\.sss$/,
        include: /src/,
        exclude: /src[\\/]assets/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              sourceMap: true,
              localIdentName: '[folder]__[local]___[hash:base64:5]',
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.css$/,
        include: /(node_modules|src[\\/]assets)/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.sss$/,
        include: /src[\\/]assets/,
        use: [
          'style-loader',
          {
            loader:'css-loader',
            options: { importLoaders: 1 }
          },
          'postcss-loader'
        ]
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ForkTsCheckerWebpackPlugin({
      tslint: true,
      memoryLimit: 4096
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true
      },
      inject: true
    })
  ]
}

module.exports = merge(common, devConfig);
